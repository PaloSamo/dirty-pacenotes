import csv
import itertools
import os
import wx
import wx.lib.agw.flatnotebook as fnb


class Manager(wx.Dialog):
    def __init__(self, parent):
        wx.Dialog.__init__(self, parent)

        self.parent = parent
        self.SetSize(wx.Size(640, 480))
        self.SetTitle('DiRTy Pacenotes Manager for ' + self.parent.co_driver)
        self.SetBackgroundColour('light grey')
        self.SetIcon(self.parent.icon)
        self.Center(wx.BOTH)
        self.SetWindowStyle(wx.DEFAULT_DIALOG_STYLE)

        self.dict_list_c = {}
        self.cat_list_c = []
        self.sound_list_c = []
        self.audio_list_c = []
        self.selection_left = []
        self.selection_right = []

        self.box_main = wx.BoxSizer(wx.HORIZONTAL)
        self.create_panel()
        self.SetSizer(self.box_main)

        self.create_audio()
        self.create_sounds()

    def create_panel(self):
        # Left side
        box_left = wx.BoxSizer(wx.HORIZONTAL)
        self.tabs_left = fnb.FlatNotebook(self, agwStyle=fnb.FNB_HIDE_ON_SINGLE_TAB)
        box_left.Add(self.tabs_left, 1, wx.EXPAND)
        self.box_main.Add(box_left, 1, wx.EXPAND | wx.ALL, 10)

        # Right side
        box_right = wx.BoxSizer(wx.VERTICAL)

        # Panel with buttons (top)
        box_but_top = wx.BoxSizer(wx.HORIZONTAL)

        self.button_in = wx.Button(self, wx.ID_ANY, label=u'IN')
        self.button_in.Bind(wx.EVT_BUTTON, self.sounds_in)
        box_but_top.Add(self.button_in, 0, wx.ALL, 5)
        self.button_out = wx.Button(self, wx.ID_ANY, label=u'OUT')
        self.button_out.Bind(wx.EVT_BUTTON, self.sounds_out)
        box_but_top.Add(self.button_out, 0, wx.ALL, 5)
        button_add = wx.Button(self, wx.ID_ANY, label='ADD CATEGORY')
        button_add.Bind(wx.EVT_BUTTON, self.add_category)
        box_but_top.Add(button_add, 0, wx.ALL, 5)

        box_right.Add(box_but_top, 0, wx.ALIGN_CENTER_HORIZONTAL)

        self.button_in.Disable()
        self.button_out.Disable()

        # Panel with categories (bottom right)
        box_cat = wx.BoxSizer(wx.HORIZONTAL)
        self.tabs_right = wx.aui.AuiNotebook(self, style=wx.aui.AUI_NB_WINDOWLIST_BUTTON | wx.aui.AUI_NB_TAB_MOVE |
                                                         wx.aui.AUI_NB_SCROLL_BUTTONS | wx.aui.AUI_NB_CLOSE_BUTTON)
        self.tabs_right.Bind(wx.aui.EVT_AUINOTEBOOK_PAGE_CLOSE, self.on_tab_close)
        box_cat.Add(self.tabs_right, 1, wx.EXPAND)

        box_right.Add(box_cat, 2, wx.EXPAND | wx.ALL, 5)

        # Panel with buttons (bottom)
        box_but_bot = wx.BoxSizer(wx.HORIZONTAL)

        button_reset = wx.Button(self, wx.ID_ANY, label='RESET SOUNDS')
        button_reset.Bind(wx.EVT_BUTTON, self.reset_sounds)
        box_but_bot.Add(button_reset, 0, wx.ALL, 5)
        button_reload = wx.Button(self, wx.ID_ANY, label='SAVE and RELOAD')
        button_reload.Bind(wx.EVT_BUTTON, self.on_reload)
        box_but_bot.Add(button_reload, 0, wx.ALL, 5)

        box_right.Add(box_but_bot, 0, wx.ALIGN_CENTER_HORIZONTAL)

        self.box_main.Add(box_right, 0, wx.EXPAND | wx.ALL, 10)

    def create_audio(self):
        self.tabs_left.DeleteAllPages()
        for s in os.listdir(self.parent.sound_path):
            (name, ext) = s.split('.')
            self.audio_list_c.append(name)
        for sublist in list(self.parent.sound_list.values()):
            for item in sublist:
                self.sound_list_c.append(item)
        audio_list_final = self.diff(self.audio_list_c, self.sound_list_c)
        audio_list_final.sort()
        tab_left = wx.Panel(self.tabs_left, style=wx.BORDER_NONE, id=1)
        tab_left.SetBackgroundColour('white')
        tab_left.SetCursor(wx.Cursor(wx.CURSOR_HAND))
        list_box_left = wx.ListBox(tab_left, choices=audio_list_final, style=wx.LB_MULTIPLE)
        h_box_tabs = wx.BoxSizer(wx.HORIZONTAL)
        h_box_tabs.Add(list_box_left, 0, wx.EXPAND)
        tab_left.SetSizer(h_box_tabs)
        list_box_left.Bind(wx.EVT_LISTBOX, self.on_listbox_left)
        self.tabs_left.AddPage(tab_left, 'audio')

    def create_sounds(self):
        self.tabs_right.DeleteAllPages()
        for category, sounds_list in list(self.parent.sound_list.items()):
            tab_right = wx.Panel(self.tabs_right, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, name=category, id=2)
            tab_right.SetBackgroundColour('white')
            tab_right.SetCursor(wx.Cursor(wx.CURSOR_HAND))
            list_box_right = wx.ListBox(tab_right, choices=sounds_list, style=wx.LB_MULTIPLE)
            h_box_tabs = wx.BoxSizer(wx.HORIZONTAL)
            h_box_tabs.Add(list_box_right, 0, wx.EXPAND)
            tab_right.SetSizer(h_box_tabs)
            list_box_right.Bind(wx.EVT_LISTBOX, self.on_listbox_right)
            self.tabs_right.AddPage(tab_right, category)

    def add_category(self, event):
        dlg = wx.TextEntryDialog(self, '', 'CATEGORY NAME')
        if dlg.ShowModal() == wx.ID_OK and dlg.GetValue():
            tab_right = wx.Panel(self.tabs_right, style=wx.TAB_TRAVERSAL | wx.BORDER_NONE, name=dlg.GetValue(),
                                 id=2)
            tab_right.SetBackgroundColour('white')
            tab_right.SetCursor(wx.Cursor(wx.CURSOR_HAND))
            list_box_right = wx.ListBox(tab_right, style=wx.LB_MULTIPLE)
            h_box_tabs = wx.BoxSizer(wx.HORIZONTAL)
            h_box_tabs.Add(list_box_right, 0, wx.EXPAND)
            tab_right.SetSizerAndFit(h_box_tabs)
            list_box_right.Bind(wx.EVT_LISTBOX, self.on_listbox_right)
            self.tabs_right.AddPage(tab_right, dlg.GetValue(), True)
        dlg.Destroy()

    def on_listbox_left(self, event):
        if event.GetExtraLong():
            self.selection_left.append(event.GetString())
        else:
            self.selection_left.remove(event.GetString())
        if self.selection_left:
            self.button_in.Enable()
        else:
            self.button_in.Disable()

    def on_listbox_right(self, event):
        if event.GetExtraLong():
            self.selection_right.append(event.GetString())
        else:
            self.selection_right.remove(event.GetString())
        if self.selection_right:
            self.button_out.Enable()
        else:
            self.button_out.Disable()

    def sounds_in(self, event):
        if self.selection_left:
            for child_right in self.tabs_right.GetCurrentPage().GetChildren():
                child_right.InsertItems(self.selection_left, child_right.GetCount())
            for child_left in self.tabs_left.GetCurrentPage().GetChildren():
                sel_list = child_left.GetSelections()
                sel_list.reverse()
                for selected in sel_list:
                    child_left.Delete(selected)
            self.selection_left.clear()
            self.button_in.Disable()
        else:
            pass

    def sounds_out(self, event):
        if self.selection_right:
            for child_left in self.tabs_left.GetCurrentPage().GetChildren():
                child_left.InsertItems(self.selection_right, child_left.GetCount())
            for child_right in self.tabs_right.GetCurrentPage().GetChildren():
                sel_list = child_right.GetSelections()
                sel_list.reverse()
                for selected in sel_list:
                    child_right.Delete(selected)
            self.selection_right.clear()
            self.button_out.Disable()
        else:
            pass

    def on_tab_close(self, event):
        sel_list = []
        children = self.tabs_right.GetCurrentPage().GetChildren()
        for child in children:
            for row in range(child.GetCount()):
                sound = child.GetString(row)
                sel_list.append(sound)
        for child_left in self.tabs_left.GetCurrentPage().GetChildren():
            child_left.InsertItems(sel_list, child_left.GetCount())

    def reset_sounds(self, event):
        self.create_audio()
        self.create_sounds()

    def diff(self, l_one, l_two):
        return list(set(l_one) - set(l_two))

    def on_reload(self, event):
        for child in self.tabs_right.GetChildren():
            category = child.GetName()
            if category != 'panel':
                for grandchild in child.GetChildren():
                    sound_list = []
                    sound_dict = {}
                    for row in range(grandchild.GetCount()):
                        sound = grandchild.GetString(row)
                        if sound:
                            sound_list.append(sound)
                    sound_list.sort()
                    sound_dict[category] = sound_list
                    self.dict_list_c.update(sound_dict)
        if not self.dict_list_c:
            wx.MessageBox('Create at least one category', 'CO-DRIVER ERROR', wx.OK | wx.ICON_ERROR)
        elif self.dict_list_c:
            check = False
            for k, v in self.dict_list_c.items():
                if not v:
                    check = True
            if check:
                wx.MessageBox('At least one category is empty', 'CO-DRIVER ERROR', wx.OK | wx.ICON_ERROR)
            else:
                keys = self.dict_list_c.keys()
                with open(self.parent.sounds_csv, 'w', newline='') as f:
                    writer = csv.writer(f, delimiter=",")
                    writer.writerow(keys)
                    writer.writerows(itertools.zip_longest(*[self.dict_list_c[key] for key in keys]))
                self.Destroy()
                self.parent.reload_sounds()
