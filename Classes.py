import sys
import wx
import wx.adv


class TextDropTarget(wx.TextDropTarget):
    def __init__(self, target):
        wx.TextDropTarget.__init__(self)
        self.target = target

    def OnDropText(self, x, y, data):
        self.target.InsertItem(sys.maxsize, data)
        return True


class TaskBar(wx.adv.TaskBarIcon):
    def __init__(self, frame):
        wx.adv.TaskBarIcon.__init__(self)

        self.frame = frame
        self.SetIcon(frame.icon, frame.title)
        self.Bind(wx.EVT_MENU, self.on_show, id=1)
        self.Bind(wx.EVT_MENU, self.on_hide, id=2)
        self.Bind(wx.EVT_MENU, self.on_close, id=3)

    def CreatePopupMenu(self):
        menu = wx.Menu()
        menu.Append(1, 'Show')
        menu.Append(2, 'Hide')
        menu.Append(3, 'Close')
        return menu

    def on_show(self, event):
        if not self.frame.IsShown():
            self.frame.Show()

    def on_hide(self, event):
        if self.frame.IsShown():
            self.frame.Hide()

    def on_close(self, event):
        self.frame.Close()
