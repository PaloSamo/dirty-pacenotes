import os
import wx


class MenuBar(wx.MenuBar):
    def __init__(self, parent, frame_name, img_path, q_del):
        super(MenuBar, self).__init__()

        self.parent = parent
        self.frame_name = frame_name
        self.img_path = img_path
        self.q_del = q_del

        self.create_panel()

    def create_panel(self):
        self.create_file()
        self.create_delay()
        self.create_help()
        if self.frame_name == 'frame_pacenotes':
            self.create_edit()
            self.create_autosave()

    def create_file(self):
        self.file_menu = wx.Menu()
        if self.frame_name == 'frame_pacenotes':
            self.menu_open = wx.MenuItem(self.file_menu, wx.ID_OPEN, wx.GetStockLabel(wx.ID_OPEN) + '\tCtrl+O',
                                         'Open existing pacenotes file')
            self.menu_open.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'open.png')))
            self.Bind(wx.EVT_MENU, self.on_open, self.menu_open)
            self.file_menu.Append(self.menu_open)

            self.menu_save = wx.MenuItem(self.file_menu, wx.ID_SAVE, wx.GetStockLabel(wx.ID_SAVE) + '\tCtrl+S',
                                         'Overwrite current pacenotes file')
            self.menu_save.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'save.png')))
            self.Bind(wx.EVT_MENU, self.on_save, self.menu_save)
            self.file_menu.Append(self.menu_save)

            self.file_menu.AppendSeparator()

            self.menu_manager = wx.MenuItem(self.file_menu, wx.ID_ANY, 'Pacenotes Manager' + '\tCtrl+M',
                                            'Organize co-driver pacenote calls')
            self.menu_manager.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'creator.png')))
            self.Bind(wx.EVT_MENU, self.on_creator, self.menu_manager)
            self.file_menu.Append(self.menu_manager)

        self.menu_settings = wx.MenuItem(self.file_menu, wx.ID_ANY, 'Service Area' + '\tCtrl+T',
                                         'Change app settings')
        self.menu_settings.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'settings.png')))
        self.Bind(wx.EVT_MENU, self.on_settings, self.menu_settings)
        self.file_menu.Append(self.menu_settings)

        self.file_menu.AppendSeparator()

        if self.parent.mode == 'Classic':
            self.menu_mode = wx.MenuItem(self.file_menu, wx.ID_ANY, 'Record Mode' + '\tCtrl+R',
                                         'Record pacenotes via microphone')
            self.menu_mode.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'recorder.png')))
        if self.parent.mode == 'Record':
            self.menu_mode = wx.MenuItem(self.file_menu, wx.ID_ANY, 'Classic Mode' + '\tCtrl+R',
                                         'Create pacenotes from sound files')
            self.menu_mode.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'mouse.png')))
        self.Bind(wx.EVT_MENU, self.on_mode, self.menu_mode)
        self.file_menu.Append(self.menu_mode)

        self.file_menu.AppendSeparator()

        if self.frame_name == 'frame_pacenotes' and self.parent.mode == 'Record':
            self.menu_manager.Enable(False)

        self.menu_quit = wx.MenuItem(self.file_menu, wx.ID_EXIT, wx.GetStockLabel(wx.ID_EXIT) + '\tCtrl+Q',
                                     'Close the app')
        self.menu_quit.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'exit.png')))
        self.Bind(wx.EVT_MENU, self.parent.on_quit, self.menu_quit)
        self.file_menu.Append(self.menu_quit)
        if self.frame_name == 'frame_pacenotes':
            self.menu_save.Enable(False)

    def create_edit(self):
        self.edit_menu = wx.Menu()
        '''
        self.menu_cut = wx.MenuItem(self.file_menu, wx.ID_CUT, wx.GetStockLabel(wx.ID_CUT) + '\tCtrl+X',
                               'Cut selected text')
        self.menu_cut.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'cut.png')))
        self.Bind(wx.EVT_TEXT_CUT, self.menu_cut)
        self.edit_menu.Append(self.menu_cut)

        self.menu_copy = wx.MenuItem(self.file_menu, wx.ID_COPY, wx.GetStockLabel(wx.ID_COPY) + '\tCtrl+C',
                                'Copy selected text')
        self.menu_copy.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'copy.png')))
        self.Bind(wx.EVT_TEXT_COPY, self.menu_copy)
        self.edit_menu.Append(self.menu_copy)

        self.menu_paste = wx.MenuItem(self.file_menu, wx.ID_PASTE, wx.GetStockLabel(wx.ID_PASTE) + '\tCtrl+V',
                                 'Paste text from clipboard')
        self.menu_paste.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'paste.png')))
        self.Bind(wx.EVT_TEXT_PASTE, self.menu_paste)
        self.edit_menu.Append(self.menu_paste)

        self.edit_menu.AppendSeparator()

        self.menu_delete = wx.MenuItem(self.file_menu, wx.ID_DELETE, wx.GetStockLabel(wx.ID_DELETE) + '\tDel',
                                  'Delete selected text')
        self.menu_delete.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'delete.png')))
        self.Bind(wx.EVT_TEXT, self.menu_delete)
        self.edit_menu.Append(self.menu_delete)
        '''
        self.menu_select_all = wx.MenuItem(self.file_menu, 20000, 'Select All',
                                           'Select all lines of pacenotes', wx.ITEM_CHECK)
        self.Bind(wx.EVT_MENU, self.parent.on_tick, self.menu_select_all)
        self.edit_menu.Append(self.menu_select_all)

    def create_autosave(self):
        self.autosave_menu = wx.Menu()
        self.radio_off = self.autosave_menu.AppendRadioItem(1000, 'OFF')
        self.radio_two = self.autosave_menu.AppendRadioItem(2, '2 min')
        self.radio_five = self.autosave_menu.AppendRadioItem(5, '5 min')
        self.radio_ten = self.autosave_menu.AppendRadioItem(10, '10 min')
        for radio in [self.radio_off, self.radio_two, self.radio_five, self.radio_ten]:
            if int(self.parent.interval) == radio.GetId():
                radio.Check()

            self.Bind(wx.EVT_MENU, self.parent.on_interval, radio)

    def create_delay(self):
        self.delay_menu = wx.Menu()
        self.delay_recce = self.delay_menu.AppendRadioItem(100, 'Recce')
        self.delay_late = self.delay_menu.AppendRadioItem(150, 'Late')
        self.delay_normal = self.delay_menu.AppendRadioItem(200, 'Normal')
        self.delay_earlier = self.delay_menu.AppendRadioItem(250, 'Earlier')
        self.delay_early = self.delay_menu.AppendRadioItem(300, 'Very Early')
        for radio in [self.delay_recce, self.delay_late, self.delay_normal, self.delay_earlier, self.delay_early]:
            if self.parent.delay == radio.GetId():
                radio.Check()
            self.Bind(wx.EVT_MENU, self.on_delay, radio)

    def create_help(self):
        self.help_menu = wx.Menu()
        self.menu_about = wx.MenuItem(self.help_menu, wx.ID_ABOUT, wx.GetStockLabel(wx.ID_ABOUT), 'About this app')
        self.menu_about.SetBitmap(wx.Bitmap(os.path.join(self.img_path, 'about.png')))
        self.Bind(wx.EVT_MENU, self.parent.on_about, self.menu_about)
        self.help_menu.Append(self.menu_about)

    def on_creator(self, event):
        self.parent.show_creator()

    def on_settings(self, event):
        self.parent.show_settings()

    def on_mode(self, event):  # NOT IN USE
        mode_l = self.menu_mode.GetItemLabelText().split()
        self.parent.mode = mode_l[0]
        self.parent.on_quit(event)
        self.parent.restart_app(self)

    def on_save(self, event):
        if event.GetId() == wx.ID_SAVE:  # From menu.
            if self.parent.checkboxes:
                if self.parent.modified:
                    self.parent.write_file()
                    self.parent.SetStatusText(self.parent.file_name + ' has been saved')
                else:
                    self.parent.SetStatusText(self.parent.file_name + ' has not been modified')
                    self.parent.error()
            else:
                if self.parent.stage_name and self.parent.modified:
                    dlg = wx.MessageDialog(self, 'Do you want to save ' + self.parent.file_name +
                                           ' without pacenotes?', 'Confirm',
                                           wx.YES_NO | wx.NO_DEFAULT | wx.ICON_WARNING)
                    dlg_choice = dlg.ShowModal()
                    if dlg_choice == wx.ID_YES:
                        self.parent.write_file()
                    dlg.Destroy()

    def on_open(self, event):
        if self.parent.stage_name:
            if self.parent.udp:
                self.parent.SetStatusText(self.parent.file_name + ' is running')
                self.parent.error()
                return
            if self.parent.checkboxes and self.parent.modified:
                dlg = wx.MessageDialog(self, 'Do you want to save ' + self.parent.file_name + '?', 'Confirm',
                                       wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING)
                dlg_choice = dlg.ShowModal()
                if dlg_choice == wx.ID_YES:
                    self.on_save(event)
        dlg = wx.FileDialog(self, 'Open pacenotes file', self.parent.pace_path, '', 'Text files (*.txt)|*.txt',
                            wx.FD_OPEN | wx.FD_FILE_MUST_EXIST)
        if dlg.ShowModal() == wx.ID_OK:
            self.parent.file_name = dlg.GetFilename()
            self.parent.stage_path = dlg.GetDirectory()
            self.parent.stage_name, ext = os.path.splitext(self.parent.file_name)
            self.parent.open_file()
        dlg.Destroy()
        self.parent.editor.label_delay.SetLabel('NOTES (no udp)')

    def on_delay(self, event):
        self.parent.delay = event.GetId()
        self.q_del.put_nowait(self.parent.delay - 100)
        self.parent.update_delay()
        delay = self.delay_menu.FindItemById(self.parent.delay).GetItemLabelText()
        self.parent.SetStatusText('Pacenote calls set to ' + delay)
