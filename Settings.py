import os
import wx
from pubsub import pub
from pathlib import Path


class Settings(wx.Dialog):
    def __init__(self, parent, frame_name, app_path):
        wx.Dialog.__init__(self, parent)

        self.parent = parent
        self.frame_name = frame_name
        self.app_path = app_path

        if self.frame_name == 'frame_pacenotes':
            self.SetSize(wx.Size(240, 540))
        else:
            self.SetSize(wx.Size(240, 400))
        self.SetTitle('DiRTy Service Area')
        self.SetIcon(self.parent.icon)
        self.Center(wx.BOTH)

        self.box_main = wx.BoxSizer(wx.VERTICAL)
        self.create_panel()
        self.SetSizer(self.box_main)

        pub.subscribe(self.get_key, 'get_key')

    def create_panel(self):
        self.create_udp_server()
        self.create_co_driver()
        if self.frame_name == 'frame_pacenotes':
            self.create_record_key()
        self.create_handbrake_key()
        self.create_countdown()
        self.create_buttons()

    def create_udp_server(self):
        box_server = wx.StaticBox(self, 0, 'UDP SERVER')
        sbs_server = wx.StaticBoxSizer(box_server)

        label_ip = wx.StaticText(self, 0, 'IP')
        sbs_server.Add(label_ip, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 10)

        self.ip_value = wx.TextCtrl(self, size=wx.Size(60, 24))
        self.ip_value.SetValue(self.parent.ip)
        sbs_server.Add(self.ip_value, 0, wx.ALL, 10)

        label_port = wx.StaticText(self, 0, 'Port')
        sbs_server.Add(label_port, 0, wx.ALIGN_CENTER_VERTICAL)

        self.port_value = wx.TextCtrl(self, size=wx.Size(50, 24))
        self.port_value.SetValue(str(self.parent.port))
        sbs_server.Add(self.port_value, 0, wx.ALL, 10)

        self.box_main.Add(sbs_server, 0, wx.EXPAND | wx.ALL, 10)

    def create_co_driver(self):
        box_co_driver = wx.StaticBox(self, 0, 'CO-DRIVER' + ' (' + self.parent.mode + ' Mode)')
        sbs_co_driver = wx.StaticBoxSizer(box_co_driver, wx.VERTICAL)

        if self.parent.co_dir:
            self.co_drivers = os.listdir(self.parent.co_dir)
        else:
            self.co_drivers = []
        self.combo_co_driver = wx.ComboBox(self, choices=self.co_drivers, style=wx.CB_READONLY)
        self.combo_co_driver.SetValue(self.parent.co_driver)
        self.combo_co_driver.SetFocus()
        sbs_co_driver.Add(self.combo_co_driver, 0, wx.EXPAND | wx.ALL, 10)

        self.box_main.Add(sbs_co_driver, 0, wx.EXPAND | wx.ALL, 10)

    def create_record_key(self):
        box_rec_key = wx.StaticBox(self, 0, 'PUSH TO RECORD')
        sbs_rec_key = wx.StaticBoxSizer(box_rec_key)

        self.rec_key_value = wx.TextCtrl(self, size=wx.Size(80, 24), style=wx.TE_READONLY, name='rec')
        self.rec_key_value.SetValue(self.parent.rec_key)
        self.rec_key_value.Disable()
        # self.rec_key_value.Bind(wx.EVT_CHAR, self.change_rec_key)  # AHK
        sbs_rec_key.Add(self.rec_key_value, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 10)
        sbs_rec_key.AddStretchSpacer(1)

        self.button_record = wx.Button(self, 11, 'CHANGE')
        self.button_record.Bind(wx.EVT_BUTTON, self.on_change_key)
        sbs_rec_key.Add(self.button_record, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, 10)

        self.box_main.Add(sbs_rec_key, 0, wx.EXPAND | wx.ALL, 10)

    def create_handbrake_key(self):
        box_han_key = wx.StaticBox(self, 0, 'HANDBRAKE')
        sbs_han_key = wx.StaticBoxSizer(box_han_key)

        self.han_key_value = wx.TextCtrl(self, size=wx.Size(80, 24), style=wx.TE_READONLY, name='han')
        self.han_key_value.SetValue(self.parent.han_key)
        self.han_key_value.Disable()
        # self.han_key_value.Bind(wx.EVT_KEY_DOWN, self.change_han_key)  # AHK
        sbs_han_key.Add(self.han_key_value, 0, wx.ALIGN_CENTER_VERTICAL | wx.ALL, 10)
        sbs_han_key.AddStretchSpacer(1)

        self.button_handbrake = wx.Button(self, wx.ID_ANY, 'CHANGE')
        self.button_handbrake.Bind(wx.EVT_BUTTON, self.on_change_key)
        sbs_han_key.Add(self.button_handbrake, 0, wx.ALIGN_CENTER_VERTICAL | wx.RIGHT, 10)

        self.box_main.Add(sbs_han_key, 0, wx.EXPAND | wx.ALL, 10)

    def create_countdown(self):  # TODO - implement config for all co-drivers separately?
        box_countdown = wx.BoxSizer(wx.HORIZONTAL)
        self.count_check = wx.CheckBox(self, 0, 'COUNTDOWN')
        box_countdown.Add(self.count_check, 0, wx.ALL, 10)
        self.count_check.SetValue(bool(self.parent.countdown))

        self.box_main.Add(box_countdown, 0, wx.ALIGN_CENTRE_HORIZONTAL)

    def create_buttons(self):
        bs_buttons = wx.BoxSizer(wx.HORIZONTAL)
        button_restart = wx.Button(self, label='SAVE and RESTART')
        button_restart.Bind(wx.EVT_BUTTON, self.parent.on_restart)
        bs_buttons.Add(button_restart, 0)

        self.box_main.Add(bs_buttons, 0, wx.ALIGN_CENTER_HORIZONTAL | wx.TOP, 20)

    def on_change_key(self, event):
        pub.subscribe(self.get_key, 'get_key')
        message = '- apply handbrake -'
        self.dlg = wx.TextEntryDialog(self, message=message, caption='INPUT')
        self.dlg.GetChildren()[1].Disable()
        if self.dlg.ShowModal() == wx.ID_OK:
            if self.dlg.GetValue():
                self.parent.han_key = self.dlg.GetValue()
                self.han_key_value.SetValue(self.parent.han_key)
            else:
                pass
        else:
            self.dlg.Destroy()

    def get_key(self, key):
        try:
            if self.dlg.IsShown():
                self.dlg.SetValue(str(key))
        except RuntimeError and AttributeError:
            pass
        pub.unsubscribe(self.get_key, 'get_key')

    def on_create(self, event):
        dlg = wx.TextEntryDialog(self, '', 'CO-DRIVER NAME')
        if dlg.ShowModal() == wx.ID_OK and dlg.GetValue():
            if dlg.GetValue() != '':
                co_driver = dlg.GetValue()
                co_path = os.path.join(self.app_path, self.parent.co_dir, co_driver)
                if co_driver not in self.co_drivers:
                    try:
                        with open(os.path.join(self.app_path, 'data\\stages.csv'), 'r') as f:
                            _ = next(f)
                            for line in f:
                                row = line.strip()
                                lis = row.partition(',')  # tuple
                                val = lis[2]  # string
                                pair = val.split(',')  # list
                                if self.parent.mode == 'Record':
                                    Path(os.path.join(co_path, 'sounds', pair[2], pair[1])).mkdir(parents=True,
                                                                                                  exist_ok=True)
                                    open(os.path.join(co_path, 'sounds', pair[2], 'sounds.csv'), 'a').close()
                                Path(os.path.join(co_path, 'pacenotes', pair[2])).mkdir(parents=True,
                                                                                        exist_ok=True)
                                open(os.path.join(co_path, 'pacenotes', pair[2], pair[1] + '.txt'), 'a').close()
                        if self.parent.mode == 'Classic':
                            Path(os.path.join(co_path, 'sounds')).mkdir(parents=True, exist_ok=True)
                            open(os.path.join(co_path, 'sounds.csv'), 'a').close()
                        self.combo_co_driver.Append(co_driver)
                        self.combo_co_driver.SetStringSelection(co_driver)
                        self.co_drivers.append(co_driver)
                    except FileExistsError:
                        pass
                else:
                    pass
        dlg.Destroy()

    def change_rec_key(self, event):  # NOT IN USE (AHK)
        self.rec_key_value.SetValue(str(event.GetKeyCode()))

    def change_han_key(self, event):  # NOT IN USE (AHK)
        self.han_key_value.SetValue(str(event.GetKeyCode()))
