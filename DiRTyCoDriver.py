"""
DiRTy Co-Driver

Copyright [2017 - 2020] [Palo Samo]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import os
import re
import socket
import struct
import sys
import ast
import math
import random
import wx
import wx.adv
import wx.aui
import wx.grid
import wx.lib.scrolledpanel as scr
from wx.lib.wordwrap import wordwrap
from collections import defaultdict, OrderedDict
from configobj import ConfigObj
from threading import Thread
from queue import Queue
from pydub import AudioSegment
from pydub import playback
from pathlib import Path
from pubsub import pub
from pyjoystick.sdl2 import Key, Joystick, run_event_loop
from Classes import TaskBar
from MenuBar import MenuBar
from Settings import Settings


app_path = os.getcwd()
data_path = os.path.join(app_path, 'data')
img_path = os.path.join(data_path, 'images')
config_ini = os.path.join(data_path, 'config.ini')
sound_bank = {}

# queues for threads
q_run = Queue()  # running
q_rst = Queue()  # reset
q_del = Queue()  # delay
q_dic = Queue()  # pacenotes


class UDP(Thread):
    def __init__(self, received):
        Thread.__init__(self)

        for r in received:
            self.server = received[0]
            self.mode = received[1]
            self.pace_path = received[2]
            self.snd_path = received[3]
            self.delay = received[4]
            self.countdown = received[5]
            self.han_key = received[6]
            self.dic_stages = received[7]

        self.dic_pacenotes = OrderedDict()
        self.dic_new_pacenotes = OrderedDict()
        self.new_dist = 0
        self.pos_y = 0
        self.curr_lap = 0
        self.total_laps = 0
        self.total_time = 0
        self.stage_length = 0
        self.stage_path = ''
        self.stage_name = ''
        self.stage_name_dic = ''
        self.stage_folder = ''
        self.stage_file = ''
        self.dist_played = 0
        self.countdown = True
        self.lap_time = 0
        self.sound_bank_keys = []
        self.hand_pulled = False
        self.speed = 0
        self.sound_pace = ''
        self.sound_wrong = ''
        self.wrong_played = False

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.server)

        self.running = True
        self.setDaemon(True)
        self.start()

    def run(self):
        while self.running:
            print('init', self.curr_lap, self.total_time)
            if self.curr_lap == 0 == self.total_time:  # car at the start but not restart
                self.receive_udp_packet()
                self.detect_stage()
                self.read_pacenotes_file()
            print('stream', self.curr_lap, self.total_time)
            self.receive_udp_stream()
        self.sock.shutdown(socket.SHUT_RD)
        self.sock.close()

    def receive_udp_packet(self):
        while True:
            print('waiting for UDP stream on ', self.server)
            udp_stream = self.sock.recv(512)
            if not udp_stream:
                print('lost connection')
                break  # lost connection
            udp_data = struct.unpack('64f', udp_stream[0:256])
            self.total_time = int(udp_data[0])
            self.pos_y = int(udp_data[5])
            curr_lap = int(udp_data[36])
            self.total_laps = int(udp_data[60])
            self.stage_length = round(udp_data[61], 4)
            if curr_lap == 1:  # Wait for udp from next stage after finish.
                print('waiting for next stage')
                continue
            break  # run once

    def detect_stage(self):
        for k, v in list(self.dic_stages.items()):
            if self.stage_length == k and self.total_laps == 1:  # rally stage indicator
                for s in v:
                    if len(v) == 1:
                        stg = s.split(',')
                        self.stage_name_dic = stg[1]
                        self.stage_folder = stg[2]
                    elif len(v) == 2:
                        stg = s.split(',')
                        pos_start = int(stg[0])
                        if self.pos_y == pos_start:
                            self.stage_name_dic = stg[1]
                            self.stage_folder = stg[2]
        if self.stage_name_dic != self.stage_name:
            self.stage_name = self.stage_name_dic
            self.stage_path = os.path.join(self.pace_path, self.stage_folder)
            self.stage_file = os.path.join(self.stage_path, self.stage_name + '.txt')
            wx.CallAfter(pub.sendMessage, 'get_stage', stage_name=self.stage_name, stage_path=self.stage_path)
            print(self.stage_name)

    def read_pacenotes_file(self):
        self.dic_pacenotes.clear()
        try:
            with open(self.stage_file, 'r') as f:
                for line in f:
                    if line and line.strip():
                        lis = line.split(',')  # list [curr_dist, sound]
                        key = int(lis[0])  # key as integer
                        val = lis[1].strip()  # value as string
                        self.dic_pacenotes[key] = []  # empty list
                        self.dic_pacenotes[key].append(val)  # dictionary
                    else:
                        continue  # skip empty lines
        except FileNotFoundError:
            wx.CallAfter(pub.sendMessage, 'key_error', arg=self.stage_file)

    def receive_udp_stream(self):
        last_dist = -20

        # Main udp loop.
        while self.running:
            if not q_run.empty():
                self.running = q_run.get_nowait()
                q_run.task_done()
            if not q_rst.empty():
                reset = q_rst.get_nowait()
                q_rst.task_done()
                if reset is True:
                    return
            if not q_del.empty():
                self.delay = q_del.get_nowait()
                q_del.task_done()
            if not q_dic.empty():
                self.dic_pacenotes.clear()
                dic_pace = q_dic.get_nowait()
                q_dic.task_done()
                for key, val in list(dic_pace.items()):
                    self.dic_pacenotes[int(key)] = []
                    self.dic_pacenotes[int(key)].append(val.strip())

            udp_stream = self.sock.recv(512)
            if not udp_stream:
                return  # lost connection
            udp_data = struct.unpack('64f', udp_stream[0:256])
            self.total_time = udp_data[0]
            lap_time = udp_data[1]
            curr_dist = int(udp_data[2])
            speed = udp_data[7]
            self.curr_lap = int(udp_data[36])
            print('main', self.curr_lap, self.total_time)
            # For handbrake countdown.
            wx.CallAfter(pub.sendMessage, 'get_udp_data', lap_time=lap_time, speed=speed)

            # Play sounds.
            if self.curr_lap == 0:  # car on stage but before finish line.
                wx.CallAfter(pub.sendMessage, 'get_dist', curr_dist=curr_dist, last_dist=last_dist)
                self.dic_new_pacenotes.clear()
                for dist, pace in list(self.dic_pacenotes.items()):
                    if curr_dist < self.delay:
                        self.new_dist = math.ceil(dist / 2)
                    elif curr_dist >= self.delay:
                        self.new_dist = dist - self.delay
                    self.dic_new_pacenotes[self.new_dist] = pace
                for new_dist, new_pace in list(self.dic_new_pacenotes.items()):
                    for i in range(6):
                        if curr_dist == new_dist + i and new_dist != self.dist_played:
                            if curr_dist > last_dist:  # play pacenotes
                                self.wrong_played = False
                                for curr_pace in new_pace:
                                    snd = curr_pace.split()
                                    for sound_base in snd:
                                        try:
                                            sound_bank_mini = [sound_base]
                                            for k in self.sound_bank_keys:
                                                if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                                    sound_bank_mini.append(k)
                                            self.sound_pace = random.choice(sound_bank_mini)
                                            playback.play(sound_bank[self.sound_pace])
                                        except KeyError:
                                            wx.CallAfter(pub.sendMessage, 'key_error', arg=self.sound_pace)
                                            pass
                        elif 0 < curr_dist < last_dist and not self.wrong_played:  # play wrong_way
                            sound_bank_keys = list(sound_bank)
                            sound_base = 'wrong_way'
                            try:
                                sound_bank_mini = [sound_base]
                                for k in sound_bank_keys:
                                    if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                        sound_bank_mini.append(k)
                                self.sound_wrong = random.choice(sound_bank_mini)
                                playback.play(sound_bank[self.sound_wrong])
                                self.wrong_played = True
                            except KeyError:
                                wx.CallAfter(pub.sendMessage, 'key_error', arg=self.sound_wrong)
                                pass
                        self.dist_played = new_dist
                        break
            elif self.curr_lap == 1:  # flying finish
                break
            last_dist = curr_dist


class JoyCatcher(Thread):
    def __init__(self):
        Thread.__init__(self)

        self.running = True
        self.setDaemon = True
        self.start()

    def run(self):
        run_event_loop(self.one, self.two, self.key_received)

    def one(self):
        pass

    def two(self):
        pass

    def key_received(self, key):
        wx.CallAfter(pub.sendMessage, 'get_key', key=key)


class AudioLoaderGen(Thread):
    def __init__(self, sound_path, snd_file_list):
        Thread.__init__(self)

        self.sound_path = sound_path
        self.snd_file_list = snd_file_list

        self.running = True
        self.setDaemon = True
        self.start()

    def run(self):
        global sound_bank
        loaded = 0
        for snd_file in self.snd_file_list:
            sound = Path(snd_file).stem
            try:
                sound_bank[sound] = AudioSegment.from_file(snd_file)
            except IndexError:
                continue
            loaded += 1
            wx.CallAfter(pub.sendMessage, 'get_progress', loaded=loaded)


class AudioLoaderPac(Thread):
    def __init__(self, sound_path, snd_file_dic):
        Thread.__init__(self)

        self.sound_path = sound_path
        self.snd_file_dic = snd_file_dic

        self.running = True
        self.setDaemon = True
        self.start()

    def run(self):
        global sound_bank
        loaded = 0
        for snd_files in self.snd_file_dic.values():
            for snd_file in snd_files:
                sound = Path(snd_file).stem
                try:
                    sound_bank[sound] = AudioSegment.from_file(snd_file)
                except IndexError:
                    continue
                loaded += 1
                wx.CallAfter(pub.sendMessage, 'get_progress', loaded=loaded)


class Editor(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.parent = parent
        self.SetBackgroundColour('white')
        self.SetWindowStyle(wx.BORDER_THEME)

        self.box_main = wx.BoxSizer(wx.VERTICAL)
        self.create_panel()
        self.SetSizer(self.box_main)

        self.count_played = False
        self.hand_pulled = False
        self.speed = 0
        self.lap_time = 0

        pub.subscribe(self.get_udp_data, 'get_udp_data')
        pub.subscribe(self.get_key, 'get_key')

    def create_panel(self):
        self.create_hand_control()  # AHK only
        self.create_scrolled_panel()
        self.create_labels()

    def create_scrolled_panel(self):
        self.box_pace = wx.StaticBox(self, 0, 'Special Stage')
        sbs_pace = wx.StaticBoxSizer(self.box_pace, wx.HORIZONTAL)

        self.v_box = wx.BoxSizer(wx.VERTICAL)

        self.scrolled_panel = scr.ScrolledPanel(self, style=wx.BORDER_NONE)
        self.scrolled_panel.SetupScrolling(scroll_x=False, rate_y=8, scrollToTop=False)
        self.scrolled_panel.SetBackgroundColour('white')
        self.scrolled_panel.SetAutoLayout(1)
        logo = wx.StaticBitmap(self.scrolled_panel)
        logo.SetBitmap(wx.Bitmap(os.path.join(img_path, 'logo.png')))
        self.scrolled_panel.SetSizer(self.v_box)
        sbs_pace.Add(self.scrolled_panel, 1, wx.EXPAND | wx.TOP, 10)

        self.box_main.Add(sbs_pace, 1, wx.EXPAND | wx.ALL, 10)

    def create_labels(self):
        self.label_length = wx.StaticText(self, size=wx.Size(78, 24), style=wx.TE_RIGHT, label='0 m')
        self.label_length.SetForegroundColour('grey')

        self.label_delay = wx.StaticText(self, label=self.parent.delay_mode)
        self.label_delay.SetForegroundColour('dark grey')

        self.label_co_driver = wx.StaticText(self, label=self.parent.co_driver)
        self.label_co_driver.SetForegroundColour('dark grey')
        self.label_co_driver.SetFont(self.parent.font.Bold())

        label_mode = wx.StaticText(self, label='|  ' + self.parent.mode + ' Mode')
        label_mode.SetForegroundColour('dark grey')

        h_box_labels = wx.FlexGridSizer(rows=1, cols=4, vgap=0, hgap=10)
        h_box_labels.AddMany([self.label_length, self.label_delay, self.label_co_driver, label_mode])
        h_box_labels.AddGrowableCol(1, 1)
        self.box_main.Add(h_box_labels, 0, wx.EXPAND | wx.LEFT | wx.RIGHT, 10)

    def get_key(self, key):
        if self.parent.han_key == key:
            self.hand_pulled = True
            self.play_countdown()
            self.hand_pulled = False

    def get_udp_data(self, lap_time, speed):
        self.lap_time = lap_time
        self.speed = speed

    def play_countdown(self):  # TODO - adjust delay for restarts when leader time shown (or make note for players)
        self.speed = 1
        if self.parent.countdown and self.hand_pulled:
            if self.lap_time == 0:
                if self.speed == 0:
                    self.count_played = False
                    return
                elif self.speed > 0 and not self.count_played:  # play countdown sound
                    self.count_played = True
                    sound_bank_keys = list(sound_bank)
                    sound_base = 'countdown_start'
                    try:
                        sound_bank_mini = [sound_base]
                        for k in sound_bank_keys:
                            if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                sound_bank_mini.append(k)
                        self.sound_name = random.choice(sound_bank_mini)
                        playback.play(sound_bank[self.sound_name])
                    except KeyError:
                        self.parent.key_error(self.sound_name)
                        pass

    # AHK only
    def create_hand_control(self):
        blank_box = wx.BoxSizer(wx.HORIZONTAL)
        self.blank_hand_input = wx.TextCtrl(self)
        self.blank_hand_input.Bind(wx.EVT_KEY_DOWN, self.on_handbrake)
        blank_box.Add(self.blank_hand_input)
        self.blank_hand_input.Hide()
        self.box_main.Add(blank_box, 0, wx.EXPAND | wx.ALL, 10)

    def on_handbrake(self, event):  # from hand control
        self.hand_pulled = True
        self.play_countdown()
        self.hand_pulled = False


class DiRTyPacenotes(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(DiRTyPacenotes, self).__init__(*args, **kwargs)

        config = self.get_config()
        self.ip = config['ip']
        self.port = int(config['port'])
        self.server = (self.ip, self.port)
        self.mode = config['mode']
        # self.co_dir = 'co-drivers'
        # self.co_driver = config['co_driver']

        self.co_driver_c = config['co_driver_c']
        self.co_driver_r = config['co_driver_r']
        if self.mode == 'Classic':
            self.co_dir = 'co-drivers'
            self.co_driver = self.co_driver_c
        elif self.mode == 'Record':
            self.co_dir = 'co-drivers-rec'
            self.co_driver = self.co_driver_r

        self.delay = int(config['delay'])
        self.interval = int(config['interval'])
        self.countdown = ast.literal_eval(config['countdown'])
        self.han_key = config['han_key']
        self.rec_key = config['rec_key']

        if self.co_driver not in os.listdir(self.co_dir):  # Remove deleted co-drivers.
            self.co_driver = ''
            self.update_config()
        self.co_path = os.path.join(app_path, self.co_dir, self.co_driver)
        self.pace_path = os.path.join(self.co_path, 'pacenotes')
        self.sound_path = os.path.join(self.co_path, 'sounds')
        self.sound_list = defaultdict(list)
        self.sounds_csv = os.path.join(self.co_path, 'sounds.csv')

        # q_udp.put_nowait((self.server, self.mode, self.pace_path, self.sound_path,
                          # self.delay - 100, self.countdown, self.han_key))

        self.SetName('frame_co_driver')
        self.title = 'DiRTy Co-Driver'
        self.icon = wx.Icon(os.path.join(img_path, 'favicon_dc.ico'))
        self.font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
        self.SetMinSize(wx.Size(480, 380))
        self.SetSize(wx.Size(480, 380))
        self.SetTitle(self.title)
        self.SetIcon(self.icon)

        self.file_handle = ''
        self.file_name = ''
        self.radios = []
        self.dist = 0
        self.pace = ''
        self.stage_name = ''
        self.stage_path = ''
        self.stage_length = ''
        self.delay_mode = ''
        self.curr_dist = 0
        self.loaded_max = 0
        self.count_error = 0
        self.count_auto = 0
        self.snd_file_list = []
        self.snd_file_dic = defaultdict(list)
        self.stage_name_full = ''

        self.timer_error = wx.Timer(self)
        self.timer_auto = wx.Timer(self)

        self.menu_bar = MenuBar(self, self.GetName(), img_path, q_del)
        self.menu_bar.Append(self.menu_bar.file_menu, '&File')
        self.menu_bar.Append(self.menu_bar.delay_menu, '&Pacenotes')
        self.menu_bar.Append(self.menu_bar.help_menu, '&Help')
        self.SetMenuBar(self.menu_bar)

        self.taskbar = TaskBar(self)

        self.statusbar = self.CreateStatusBar()
        self.statusbar.SetName('status')

        self.joy = JoyCatcher()

        self.dic_stages = defaultdict(list)
        self.read_stages_csv()
        print(self.snd_file_dic)
        # Load AudioSegments into RAM
        self.scan_sounds()
        self.loader_generic = AudioLoaderGen(self.sound_path, self.snd_file_list)
        if self.mode == 'Record':
            # countries = len(next(os.walk(self.sound_path))[1])
            # for c in range(countries):
            self.loader_pacenotes = AudioLoaderPac(self.sound_path, self.snd_file_dic)

        self.progress = wx.Gauge(self.statusbar, pos=(265, 4), range=self.loaded_max)

        data_for_udp = (self.server, self.mode, self.pace_path, self.sound_path,
                       self.delay - 100, self.countdown, self.han_key, self.dic_stages)
        self.reader = UDP(data_for_udp)

        self.editor = Editor(self)

        if not self.co_driver:  # First run.
            self.show_settings()

        pub.subscribe(self.get_progress, 'get_progress')
        pub.subscribe(self.get_stage, 'get_stage')
        pub.subscribe(self.key_error, 'key_error')

        self.Bind(wx.EVT_TIMER, self.on_timer_error, self.timer_error)
        self.Bind(wx.EVT_CLOSE, self.on_quit)

    def get_config(self):
        if not os.path.exists(config_ini):
            self.create_config()
        return ConfigObj(config_ini)

    def create_config(self):  # Set default values for config.ini.
        config = ConfigObj(config_ini)
        config['ip'] = '127.0.0.1'
        config['port'] = '20777'
        config['mode'] = 'Classic'
        config['co_driver_c'] = ''
        config['co_driver_r'] = ''
        config['delay'] = '200'
        config['interval'] = '1000'
        config['rec_key'] = 'r'
        config['han_key'] = 'h'
        config['countdown'] = 'True'
        config.write()

    def update_config(self):
        config = ConfigObj(config_ini)
        config['ip'] = self.ip
        config['port'] = self.port
        config['mode'] = self.mode
        config['co_driver_c'] = self.co_driver_c
        config['co_driver_r'] = self.co_driver_r
        config['delay'] = self.delay
        config['interval'] = self.interval
        config['rec_key'] = self.rec_key
        config['han_key'] = self.han_key
        config['countdown'] = self.countdown
        config.write()

    def show_settings(self):
        self.settings = Settings(self, self.GetName(), img_path)
        self.settings.ShowModal()

    def get_progress(self, loaded):
        self.progress.SetValue(loaded)
        if loaded == self.loaded_max:
            self.progress.Destroy()
            self.SetStatusText(self.co_driver + ' is ready')

    def read_stages_csv(self):
        try:
            with open(os.path.join(app_path, 'data\\stages.csv'), 'r') as f:
                _ = next(f)
                for line in f:
                    row = line.strip()
                    lis = row.partition(',')  # tuple
                    key = float(lis[0])  # key as float (distance)
                    val = lis[2]  # value as string (stage, country, province)
                    self.dic_stages[key].append(val)  # dictionary
        except FileNotFoundError:
            self.SetStatusText('stages.csv file not found')
            self.error()

    def get_stage(self, stage_name, stage_path):
        self.stage_name = stage_name
        self.stage_path = stage_path
        self.file_name = self.stage_name + '.txt'
        self.update_stage_name()
        self.update_delay()

    def update_stage_name(self):
        try:
            for k, v in list(self.dic_stages.items()):
                for s in v:
                    stg = s.split(',')
                    if self.stage_name == stg[1]:
                        self.stage_name_full = stg[1] + ', ' + stg[3] + ', ' + stg[2]
                        self.editor.label_length.SetLabel(str(round(k)) + ' m')
                '''
                if len(v) == 1:
                    stg = s.split(',')
                    if self.stage_name == stg[1]:
                        self.stage_name_full = stg[1] + ', ' + stg[3] + ', ' + stg[2]
                elif len(v) == 2:
                    stg = s.split(',')
                    pos_start = int(stg[0])
                    if self.pos_y == pos_start:
                        self.stage_name_dic = stg[1]
                        self.stage_folder = stg[2]
                '''
            self.editor.box_pace.SetLabel(self.stage_name_full)

        except AttributeError:
            self.SetStatusText(self.stage_name + ' is not a valid stage')
            self.error()

    def update_delay(self):
        if self.delay == 100:
            self.delay_mode = 'RECCE'
        elif self.delay == 150:
            self.delay_mode = 'STAGE (Late)'
        elif self.delay == 200:
            self.delay_mode = 'STAGE (Normal)'
        elif self.delay == 250:
            self.delay_mode = 'STAGE (Earlier)'
        elif self.delay == 300:
            self.delay_mode = 'STAGE (Very Early)'
        self.editor.label_delay.SetLabel(self.delay_mode)

    def scan_sounds(self):
        self.statusbar.SetStatusText('Processing audio files, please wait...')
        snd_total = []
        for country in os.listdir(self.sound_path):
            if os.path.isfile(os.path.join(self.sound_path, country)):  # generic sounds only
                self.snd_file_list.append(os.path.join(self.sound_path, country))
            else:  # pacenotes sounds
                for stage in os.listdir(os.path.join(self.sound_path, country)):
                    if os.path.isdir(os.path.join(self.sound_path, country, stage)):
                        for sound in os.listdir(os.path.join(self.sound_path, country, stage)):
                            if os.path.isfile(os.path.join(self.sound_path, country, stage, sound)):
                                self.snd_file_dic[stage].append(os.path.join(self.sound_path, country, stage, sound))
                                snd_total.append(sound)
        if len(snd_total) > len(self.snd_file_list):
            self.loaded_max = len(snd_total)
        else:
            self.loaded_max = len(self.snd_file_list)

    def error(self):
        self.statusbar.SetBackgroundColour('RED')
        self.statusbar.Refresh()
        self.timer_error.Start(50)

    def key_error(self, arg):
        self.statusbar.SetStatusText('\'' + arg + '\'' + ' not found in ' + self.co_driver + '/sounds folder')
        self.error()

    def on_timer_error(self, event):
        self.count_error = self.count_error + 1
        if self.count_error == 25:
            self.statusbar.SetBackgroundColour('white')
            self.statusbar.Refresh()
            self.timer_error.Stop()
            self.count_error = 0

    def on_about(self, event):
        description = wordwrap('DiRTy Co-Driver is a standalone audio player\n'
                               'for DiRT Rally and DiRT Rally 2.0 special stages\n'
                               'using pacenotes created in DiRTy Pacenotes\n', 420, wx.ClientDC(self))
        licence = wordwrap('Licensed under the Apache License, Version 2.0 (the "License");\n'
                           'you may not use this software except in compliance with the License.\n'
                           'You may obtain a copy of the License at\n'
                           'http://www.apache.org/licenses/LICENSE-2.0\n'
                           'Unless required by applicable law or agreed to in writing,\n'
                           'software distributed under the License is distributed on an "AS IS" BASIS,\n'
                           'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,\n'
                           'either express or implied. See the License for the specific language\n'
                           'governing permissions and limitations under the License.', 420, wx.ClientDC(self))
        info = wx.adv.AboutDialogInfo()
        info.SetName('DiRTy Co-Driver')
        info.SetVersion('0.5')
        info.SetDescription(description)
        info.SetCopyright('(C) 2017 - 2020 Palo Samo')
        info.SetLicence(licence)
        wx.adv.AboutBox(info)

    def on_quit(self, event):
        pub.unsubAll()
        udp_running = False
        q_run.put_nowait(udp_running)
        self.update_config()
        self.taskbar.Destroy()
        self.Destroy()

    def on_restart(self, event):  # gather data, update config file and restart app
        if self.settings.ip_value.GetValue():
            self.ip = self.settings.ip_value.GetValue()
        else:
            wx.MessageBox('IP is missing', 'MISSING VALUE', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.port_value.GetValue():
            self.port = self.settings.port_value.GetValue()
        else:
            wx.MessageBox('Port is missing', 'MISSING VALUE', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.combo_co_driver.GetValue():
            self.co_driver = self.settings.combo_co_driver.GetValue()
        else:
            wx.MessageBox('Co-Driver is missing', 'VALUE MISSING', wx.OK | wx.ICON_WARNING)
            return
        self.countdown = self.settings.count_check.GetValue()
        if self.mode == 'Classic':
            self.co_driver_c = self.co_driver
        elif self.mode == 'Record':
            self.co_driver_r = self.co_driver

        if self.co_driver:
            self.on_quit(event)
            self.restart_app(self)
        else:  # First run.
            if not self.co_driver:
                wx.MessageBox('Choose your co-driver', 'CO-DRIVER OPTION', wx.OK | wx.ICON_WARNING)
                return
            self.update_config()
            self.settings.Destroy()
            self.on_quit(event)

    @staticmethod
    def restart_app(self):
        sys.stdout.flush()
        os.execl(sys.executable, sys.executable, *sys.argv)

    # TODO - load generic sounds (Classic) and sounds from stages (Record) into RAM in separate threads at startup
    # TODO - sounds when stationary car on stage
    # TODO - sounds when stage is finished
    # TODO - stop playback when not needed
    # TODO - start minimized
    # TODO - tooltip with co-driver and stage info
    # TODO - various statusbar messages while loading sounds (funny?)


if __name__ == '__main__':
    app = wx.App()
    frame = DiRTyPacenotes(None)
    frame.Centre()
    frame.Show()
    app.MainLoop()
