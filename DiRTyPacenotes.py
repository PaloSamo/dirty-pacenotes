"""
DiRTy Pacenotes

Copyright [2017 - 2020] [Palo Samo]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
"""

import csv
import glob
import os
import re
import socket
import struct
import sys
import ast
import math
import random
import pyaudio
import wave
import wx
import wx.adv
import wx.aui
import wx.grid
import wx.lib.intctrl as intctrl
import wx.lib.scrolledpanel as scr
import wx.lib.agw.ultimatelistctrl as ulc
import wx.lib.agw.persist as per
from wx.lib.wordwrap import wordwrap
from pubsub import pub
from collections import defaultdict, OrderedDict
from configobj import ConfigObj
from threading import Thread
from queue import Queue
from pydub import AudioSegment
from pydub import playback
from pathlib import Path
from typing import Any
from pyjoystick.sdl2 import Key, Joystick, run_event_loop
from Classes import TaskBar, TextDropTarget
from MenuBar import MenuBar
from Manager import Manager
from Settings import Settings

app_path = os.getcwd()
data_path = os.path.join(app_path, 'data')
img_path = os.path.join(data_path, 'images')
config_ini = os.path.join(data_path, 'config.ini')
sound_bank = {}

# queues for threads
q_run = Queue()  # running
q_rst = Queue()  # reset
q_del = Queue()  # delay
q_dic = Queue()  # pacenotes
q_udp = Queue()  # udp
q_stg = Queue()  # stages
q_rec = Queue()  # recorder NOT IN USE
q_han = Queue()  # handbrake
q_count = Queue()


class UDP(Thread):
    def __init__(self):
        Thread.__init__(self)

        if not q_udp.empty():
            received = q_udp.get_nowait()
            q_udp.task_done()
            self.server = received[0]
            self.mode = received[1]
            self.pace_path = received[2]
            self.snd_path = received[3]
            self.delay = received[4]
            self.countdown = received[5]
            self.han_key = received[6]

        if not q_stg.empty():
            self.dic_stages = q_stg.get_nowait()
            q_stg.task_done()

        # global speed
        self.dic_pacenotes = OrderedDict()
        self.dic_new_pacenotes = OrderedDict()
        self.new_dist = 0
        self.pos_y = 0
        self.curr_lap = 0
        self.total_laps = 0
        self.total_time = 0
        self.stage_length = 0
        self.snd_ext = ''
        self.stage_path = ''
        self.stage_name = ''
        self.stage_name_dic = ''
        self.stage_folder = ''
        self.stage_file = ''
        self.dist_played = 0
        self.countdown = True
        self.lap_time = 0
        self.sound_bank_keys = []
        self.hand_pulled = False
        self.speed = 0
        self.sound_pace = ''
        self.sound_wrong = ''
        self.wrong_played = False

        self.sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.server)

        self.running = True
        self.setDaemon(True)
        self.start()

    def run(self):
        while self.running:
            if self.total_time == 0 == self.curr_lap:  # car at the start but not restart
                self.receive_udp_packet()
                self.detect_stage()
                self.read_pacenotes_file()
            self.receive_udp_stream()
        self.sock.shutdown(socket.SHUT_RD)
        self.sock.close()

    def receive_udp_packet(self):
        while True:
            print('waiting for UDP stream on ', self.server)
            udp_stream = self.sock.recv(512)
            if not udp_stream:
                print('lost connection')
                break  # lost connection
            udp_data = struct.unpack('64f', udp_stream[0:256])
            self.total_time = int(udp_data[0])
            self.pos_y = int(udp_data[5])
            curr_lap = int(udp_data[36])
            self.total_laps = int(udp_data[60])
            self.stage_length = round(udp_data[61], 4)
            if curr_lap == 1:  # Wait for udp from next stage after finish.
                print('waiting for next stage')
                continue
            break  # run once

    def detect_stage(self):
        for k, v in list(self.dic_stages.items()):
            if self.stage_length == k and self.total_laps == 1:  # rally stage indicator
                for s in v:
                    if len(v) == 1:
                        stg = s.split(',')
                        self.stage_name_dic = stg[1]
                        self.stage_folder = stg[2]
                    elif len(v) == 2:
                        stg = s.split(',')
                        pos_start = int(stg[0])
                        if self.pos_y == pos_start:
                            self.stage_name_dic = stg[1]
                            self.stage_folder = stg[2]
        if self.stage_name_dic != self.stage_name:
            self.stage_name = self.stage_name_dic
            self.stage_path = os.path.join(self.pace_path, self.stage_folder)
            self.stage_file = os.path.join(self.stage_path, self.stage_name + '.txt')
            wx.CallAfter(pub.sendMessage, 'get_stage', stage_name=self.stage_name, stage_path=self.stage_path)

    def read_pacenotes_file(self):
        self.dic_pacenotes.clear()
        try:
            with open(self.stage_file, 'r') as f:
                for line in f:
                    if line and line.strip():
                        lis = line.split(',')  # list [curr_dist, sound]
                        key = int(lis[0])  # key as integer
                        val = lis[1].strip()  # value as string
                        self.dic_pacenotes[key] = []  # empty list
                        self.dic_pacenotes[key].append(val)  # dictionary
                    else:
                        continue  # skip empty lines
        except FileNotFoundError:
            wx.CallAfter(pub.sendMessage, 'key_error', arg=self.stage_file)

    def receive_udp_stream(self):
        last_dist = -20

        # Main udp loop.
        while self.running:
            if not q_run.empty():
                self.running = q_run.get_nowait()
                q_run.task_done()
            if not q_rst.empty():
                reset = q_rst.get_nowait()
                q_rst.task_done()
                if reset is True:
                    return
            if not q_del.empty():
                self.delay = q_del.get_nowait()
                q_del.task_done()
            if not q_dic.empty():
                self.dic_pacenotes.clear()
                dic_pace = q_dic.get_nowait()
                q_dic.task_done()
                for key, val in list(dic_pace.items()):
                    self.dic_pacenotes[int(key)] = []
                    self.dic_pacenotes[int(key)].append(val.strip())

            udp_stream = self.sock.recv(512)
            if not udp_stream:
                return  # lost connection
            udp_data = struct.unpack('64f', udp_stream[0:256])
            self.total_time = udp_data[0]
            lap_time = udp_data[1]
            curr_dist = int(udp_data[2])
            speed = udp_data[7]
            self.curr_lap = int(udp_data[36])

            # For handbrake countdown.
            wx.CallAfter(pub.sendMessage, 'get_udp_data', lap_time=lap_time, speed=speed)

            # Play sounds.
            if self.curr_lap == 0:  # car on stage but before finish line.
                wx.CallAfter(pub.sendMessage, 'get_dist', curr_dist=curr_dist, last_dist=last_dist)
                self.dic_new_pacenotes.clear()
                for dist, pace in list(self.dic_pacenotes.items()):
                    if curr_dist < self.delay:
                        self.new_dist = math.ceil(dist / 2)
                    elif curr_dist >= self.delay:
                        self.new_dist = dist - self.delay
                    self.dic_new_pacenotes[self.new_dist] = pace
                for new_dist, new_pace in list(self.dic_new_pacenotes.items()):
                    for i in range(6):
                        if curr_dist == new_dist + i and new_dist != self.dist_played:
                            if curr_dist > last_dist:  # play pacenotes
                                self.wrong_played = False
                                for curr_pace in new_pace:
                                    snd = curr_pace.split()
                                    for sound_base in snd:
                                        try:
                                            sound_bank_mini = [sound_base]
                                            for k in self.sound_bank_keys:
                                                if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                                    sound_bank_mini.append(k)
                                            self.sound_pace = random.choice(sound_bank_mini)
                                            playback.play(sound_bank[self.sound_pace])
                                        except KeyError:
                                            wx.CallAfter(pub.sendMessage, 'key_error', arg=self.sound_pace)
                                            pass
                        elif 0 < curr_dist < last_dist and not self.wrong_played:  # play wrong_way
                            sound_bank_keys = list(sound_bank)
                            sound_base = 'wrong_way'
                            try:
                                sound_bank_mini = [sound_base]
                                for k in sound_bank_keys:
                                    if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                        sound_bank_mini.append(k)
                                self.sound_wrong = random.choice(sound_bank_mini)
                                playback.play(sound_bank[self.sound_wrong])
                                self.wrong_played = True
                            except KeyError:
                                wx.CallAfter(pub.sendMessage, 'key_error', arg=self.sound_wrong)
                                pass
                        self.dist_played = new_dist
                        break
            else:  # stage finished
                break
            last_dist = curr_dist


class JoyCatcher(Thread):
    def __init__(self):
        Thread.__init__(self)

        self.running = True
        self.setDaemon = True
        self.start()

    def run(self):
        run_event_loop(self.one, self.two, self.key_received)

    def one(self):
        pass

    def two(self):
        pass

    def key_received(self, key):
        wx.CallAfter(pub.sendMessage, 'get_key', key=key)


class AudioLoader(Thread):
    def __init__(self, snd_file_list):
        Thread.__init__(self)

        self.snd_file_list = snd_file_list
        self.running = True
        self.setDaemon = True
        self.start()

    def run(self):
        if self.snd_file_list:
            loaded = 0
            for snd_file in self.snd_file_list:
                sound = Path(snd_file).stem
                snd, self.snd_ext = os.path.splitext(snd_file)
                try:
                    sound_bank[sound] = AudioSegment.from_file(snd_file)
                except IndexError:
                    continue
                loaded += 1
                wx.CallAfter(pub.sendMessage, 'get_progress', loaded=loaded)
        else:
            wx.CallAfter(pub.sendMessage, 'get_progress', loaded=0)


class AudioPlayer(Thread):  # NOT IN USE
    def __init__(self):
        Thread.__init__(self)

        self.running = True
        self.setDaemon(True)
        self.start()

    def run(self):
        while self.running:
            if not q_count.empty():
                sound = q_count.get_nowait()
                q_count.task_done()
                playback.play(sound)


class AudioRecorder(Thread):  # NOT IN USE
    def __init__(self):
        Thread.__init__(self)

        if not q_rec.empty():
            received = q_rec.get_nowait()
            q_rec.task_done()
            self.mode = received[0]
            self.co_path = received[1]
            self.pace_path = received[2]
            self.snd_path = received[3]
            self.countdown = received[4]
            self.rec_key = received[5]

        self.threshold = 250
        self.chunk = 1024
        self.format = pyaudio.paInt16
        self.channels = 2
        self.rate = 44100
        self.tally = 0
        self.data_list = []
        self.p = pyaudio.PyAudio()

        self.is_recording = False
        self.enable_trigger_record = True

        self.setDaemon(True)
        self.start()

    def is_silent(snd_data):
        # Returns 'True' if below the 'silent' threshold
        return max(snd_data) < self.threshold

    def normalize(snd_data):
        # Average the volume out
        maximum = 16384
        times = float(maximum) / max(abs(i) for i in snd_data)

        r = array('h')
        for i in snd_data:
            r.append(int(i * times))
        return r

    def trim(snd_data):
        # Trim the blank spots at the start and end
        def _trim(snd_data):
            snd_started = False
            r = array('h')

            for i in snd_data:
                if not snd_started and abs(i) > self.threshold:
                    snd_started = True
                    r.append(i)

                elif snd_started:
                    r.append(i)
            return r

        # Trim to the left
        snd_data = _trim(snd_data)

        # Trim to the right
        snd_data.reverse()
        snd_data = _trim(snd_data)
        snd_data.reverse()
        return snd_data

    def add_silence(snd_data, seconds):
        # Add silence to the start and end of 'snd_data' of length 'seconds' (float)
        silence = [0] * int(seconds * RATE)
        r = array('h', silence)
        r.extend(snd_data)
        r.extend(silence)
        return r

    def record():
        """
        Record a word or words from the microphone and
        return the data as an array of signed shorts.

        Normalizes the audio, trims silence from the
        start and end, and pads with 0.5 seconds of
        blank sound to make sure VLC et al can play
        it without getting chopped off.
        """
        p = pyaudio.PyAudio()
        stream = p.open(format=FORMAT,
                        channels=CHANNELS,
                        rate=RATE,
                        input=True,
                        output=True,
                        frames_per_buffer=CHUNK_SIZE)

        num_silent = 0
        snd_started = False

        r = array('h')

        while 1:
            # little endian, signed short
            snd_data = array('h', stream.read(CHUNK_SIZE))
            if byteorder == 'big':
                snd_data.byteswap()
            r.extend(snd_data)

            silent = is_silent(snd_data)

            if silent and snd_started:
                num_silent += 1
            elif not silent and not snd_started:
                snd_started = True

            if snd_started and num_silent > 30:
                break

        sample_width = p.get_sample_size(FORMAT)
        stream.stop_stream()
        stream.close()
        p.terminate()

        r = normalize(r)
        r = trim(r)
        r = add_silence(r, 0.5)
        return sample_width, r

    def record_to_file(path):
        # Records from the microphone and outputs the resulting data to 'path'
        sample_width, data = record()
        data = pack('<' + ('h' * len(data)), *data)

        wf = wave.open(path, 'wb')
        wf.setnchannels(CHANNELS)
        wf.setsampwidth(sample_width)
        wf.setframerate(RATE)
        wf.writeframes(data)
        wf.close()


class Editor(wx.Panel):
    def __init__(self, parent):
        wx.Panel.__init__(self, parent)

        self.parent = parent
        self.SetBackgroundColour('white')
        self.SetWindowStyle(wx.BORDER_THEME)

        self.box_main = wx.BoxSizer(wx.VERTICAL)
        self.create_panel()
        self.SetSizer(self.box_main)

        self.count_played = False
        self.hand_pulled = False
        self.speed = 0
        self.lap_time = 0
        pub.subscribe(self.get_udp_data, 'get_udp_data')

    # Define Methods.
    def create_panel(self):
        self.create_hand_control()
        self.create_rec_control()
        self.create_scrolled_panel()
        self.create_buttons()
        self.create_input_boxes()
        self.create_labels()
        self.create_tabs_notebook()

    def create_hand_control(self):
        blank_box = wx.BoxSizer(wx.HORIZONTAL)
        self.blank_hand_input = wx.TextCtrl(self)
        self.blank_hand_input.Bind(wx.EVT_KEY_DOWN, self.on_handbrake)
        blank_box.Add(self.blank_hand_input)
        self.blank_hand_input.Hide()
        self.box_main.Add(blank_box, 0, wx.EXPAND | wx.ALL, 10)

    def create_rec_control(self):
        blank_box = wx.BoxSizer(wx.HORIZONTAL)
        self.blank_rec_input = wx.TextCtrl(self)
        self.blank_rec_input.Bind(wx.EVT_KEY_DOWN, self.on_record)
        blank_box.Add(self.blank_rec_input)
        self.blank_rec_input.Hide()
        self.box_main.Add(blank_box, 0, wx.EXPAND | wx.ALL, 10)

    def create_scrolled_panel(self):
        self.box_pace = wx.StaticBox(self, 0, 'Special Stage')
        sbs_pace = wx.StaticBoxSizer(self.box_pace, wx.HORIZONTAL)

        self.v_box = wx.BoxSizer(wx.VERTICAL)

        self.scrolled_panel = scr.ScrolledPanel(self, style=wx.BORDER_NONE)
        self.scrolled_panel.SetupScrolling(scroll_x=False, rate_y=8, scrollToTop=False)
        self.scrolled_panel.SetBackgroundColour('white')
        self.scrolled_panel.SetAutoLayout(1)
        logo = wx.StaticBitmap(self.scrolled_panel)
        logo.SetBitmap(wx.Bitmap(os.path.join(img_path, 'logo.png')))
        self.scrolled_panel.SetSizer(self.v_box)
        sbs_pace.Add(self.scrolled_panel, 1, wx.EXPAND | wx.TOP, 10)

        self.box_main.Add(sbs_pace, 1, wx.EXPAND | wx.ALL, 10)

    def create_buttons(self):
        h_box_buttons = wx.BoxSizer(wx.HORIZONTAL)
        self.button_add = wx.Button(self, label='ADD')
        self.Bind(wx.EVT_BUTTON, self.parent.on_add, self.button_add)
        h_box_buttons.Add(self.button_add, 0, wx.RIGHT, 14)

        self.button_insert = wx.Button(self, label='INSERT')
        self.Bind(wx.EVT_BUTTON, self.parent.on_insert, self.button_insert)
        h_box_buttons.Add(self.button_insert, 0, wx.RIGHT, 14)

        self.button_replace = wx.Button(self, label='REPLACE')
        self.Bind(wx.EVT_BUTTON, self.parent.on_replace, self.button_replace)
        h_box_buttons.Add(self.button_replace, 0, wx.RIGHT, 14)

        self.button_delete = wx.Button(self, label='DELETE')
        self.Bind(wx.EVT_BUTTON, self.parent.on_delete, self.button_delete)
        h_box_buttons.Add(self.button_delete, 0)

        # self.button_undo_pace = but.GenBitmapButton(self, bitmap=wx.Bitmap(
        #     os.path.join(img_path, 'undo.png')), size=(25, 25))
        # h_box_buttons.Add(self.button_undo_pace, 0, wx.RIGHT, 10)
        self.buttons = (self.button_add, self.button_insert, self.button_replace, self.button_delete)
        for button in self.buttons:
            button.Disable()

        self.box_main.Add(h_box_buttons, 0, wx.ALIGN_CENTER_HORIZONTAL)

    def create_input_boxes(self):
        h_box_input = wx.BoxSizer(wx.HORIZONTAL)

        self.button_play = wx.Button(self)
        self.button_play.Bind(wx.EVT_BUTTON, self.on_play)
        self.button_play.SetInitialSize(wx.Size(24, 24))
        self.button_play.SetBitmap(wx.Bitmap(os.path.join(img_path, 'sound_on.png')))
        # button_sound.SetBitmapPressed(wx.Bitmap(os.path.join(img_path, 'sound_off.png')))
        self.button_play.Disable()
        h_box_input.Add(self.button_play, 0)

        self.input_dist = intctrl.IntCtrl(self, name='input', style=wx.TE_RIGHT, size=wx.Size(46, 24),
                                          min=-19999, max=19999, limited=True, allow_none=False)
        self.input_dist.Bind(wx.lib.intctrl.EVT_INT, self.parent.on_distance)
        self.input_dist.Disable()
        # self.input_dist.Bind(wx.EVT_KEY_UP, self.on_distance)  # Keyboard.
        # self.input_dist.Bind(wx.EVT_TEXT, self.on_distance)  # UDP stream.
        h_box_input.Add(self.input_dist, 0, wx.RIGHT, 1)

        self.input_pace = wx.SearchCtrl(self, style=wx.TE_READONLY, size=wx.Size(0, 24))
        self.input_pace.Bind(wx.EVT_SEARCHCTRL_CANCEL_BTN, self.parent.on_cancel)
        self.input_pace.SetCancelBitmap(wx.Bitmap(os.path.join(img_path, 'clear.png')))
        self.input_pace.SetCursor(wx.Cursor(wx.CURSOR_ARROW))
        self.input_pace.ShowSearchButton(False)
        self.input_pace.ShowCancelButton(True)
        self.input_pace.SetHint('pacenotes')
        h_box_input.Add(self.input_pace, 1)

        self.box_main.Add(h_box_input, 0, wx.EXPAND | wx.ALL, 10)

    def create_labels(self):
        self.label_length = wx.StaticText(self, size=wx.Size(78, 24), style=wx.TE_RIGHT)
        self.label_length.SetForegroundColour('grey')

        self.label_delay = wx.StaticText(self, label=self.parent.delay_mode)
        self.label_delay.SetForegroundColour('dark grey')

        self.label_co_driver = wx.StaticText(self, label=self.parent.co_driver)
        self.label_co_driver.SetForegroundColour('dark grey')
        self.label_co_driver.SetFont(self.parent.font.Bold())

        label_mode = wx.StaticText(self, label='|  ' + self.parent.mode + ' Mode')
        label_mode.SetForegroundColour('dark grey')

        h_box_labels = wx.FlexGridSizer(rows=1, cols=4, vgap=0, hgap=10)
        h_box_labels.AddMany([self.label_length, self.label_delay, self.label_co_driver, label_mode])
        h_box_labels.AddGrowableCol(1, 1)
        self.box_main.Add(h_box_labels, 0, wx.EXPAND | wx.LEFT | wx.RIGHT, 10)

    def create_tabs_notebook(self):
        self.tabs = wx.aui.AuiNotebook(self, style=wx.aui.AUI_NB_WINDOWLIST_BUTTON | wx.aui.AUI_NB_SCROLL_BUTTONS)
        # self.tabs.SetName('Sounds')
        self.box_main.Add(self.tabs, 1, wx.EXPAND | wx.RIGHT | wx.LEFT | wx.BOTTOM, 10)

        # TODO - adjust Editor for Record Mode (if it comes to existence)

    def on_handbrake(self, event):  # from blank control
        self.hand_pulled = True
        self.play_countdown()
        self.hand_pulled = False

    def get_udp_data(self, lap_time, speed):
        self.lap_time = lap_time
        self.speed = speed

    def play_countdown(self):  # TODO - adjust delay for restarts when leader time shown (or make note for players)
        if self.parent.countdown and self.hand_pulled:
            if self.lap_time == 0:
                if self.speed == 0:
                    self.count_played = False
                    return
                elif self.speed > 0 and not self.count_played:  # play countdown sound
                    self.count_played = True
                    sound_bank_keys = list(sound_bank)
                    sound_base = 'countdown_start'
                    try:
                        sound_bank_mini = [sound_base]
                        for k in sound_bank_keys:
                            if re.search(re.escape(sound_base) + '~' + r'\d', k, re.IGNORECASE):
                                sound_bank_mini.append(k)
                        self.sound_name = random.choice(sound_bank_mini)
                        playback.play(sound_bank[self.sound_name])
                    except KeyError:
                        self.parent.key_error(self.sound_name)
                        pass

    def on_record(self, event):
        pass

    def on_play(self, event=None):
        snd = self.input_pace.GetValue().split()
        for sound_name in snd:
            try:
                playback.play(sound_bank[sound_name])
                # audio_player = AudioPlayer(sound_bank[sound_name])
            except KeyError:
                self.parent.key_error(sound_name)


class DiRTyPacenotes(wx.Frame):
    def __init__(self, *args, **kwargs):
        super(DiRTyPacenotes, self).__init__(*args, **kwargs)

        config = self.get_config()
        self.ip = config['ip']
        self.port = int(config['port'])
        self.server = (self.ip, self.port)
        self.mode = config['mode']
        # self.co_dir = 'co-drivers'
        # self.co_driver = config['co_driver']

        self.co_driver_c = config['co_driver_c']
        self.co_driver_r = config['co_driver_r']
        if self.mode == 'Classic':
            self.co_dir = 'co-drivers'
            self.co_driver = self.co_driver_c
        elif self.mode == 'Record':
            self.co_dir = 'co-drivers-rec'
            self.co_driver = self.co_driver_r

        self.delay = int(config['delay'])
        self.interval = int(config['interval'])
        self.countdown = ast.literal_eval(config['countdown'])
        self.han_key = config['han_key']
        self.rec_key = config['rec_key']

        if self.co_driver not in os.listdir(self.co_dir):  # Remove deleted co-drivers.
            self.co_driver = ''
            self.update_config()
        self.co_path = os.path.join(app_path, self.co_dir, self.co_driver)
        self.pace_path = os.path.join(self.co_path, 'pacenotes')
        self.sound_path = os.path.join(self.co_path, 'sounds')
        self.sound_list = defaultdict(list)
        self.sounds_csv = os.path.join(self.co_path, 'sounds.csv')

        q_udp.put_nowait((self.server, self.mode, self.pace_path, self.sound_path,
                          self.delay - 100, self.countdown, self.han_key))
        # q_rec.put_nowait((self.mode, self.co_path, self.pace_path, self.sound_path, self.countdown, self.rec_key_q))

        self.SetName('frame_pacenotes')
        self.title = 'DiRTy Pacenotes'
        self.icon = wx.Icon(os.path.join(img_path, 'favicon_dp.ico'))
        self.font = wx.SystemSettings.GetFont(wx.SYS_DEFAULT_GUI_FONT)
        self.SetMinSize(wx.Size(480, 360))
        self.SetSize(wx.Size(480, 720))
        self.SetTitle(self.title)
        self.SetIcon(self.icon)

        self.file_handle = ''
        self.file_name = ''
        self.radios = []
        self.dist = 0
        self.pace = ''
        self.stage_name = ''
        self.stage_path = ''
        self.stage_length = ''
        self.delay_mode = ''
        self.curr_dist = 0
        self.curr_line = None
        self.prev_line = None
        self.last_dist = -20
        self.cbs = set()
        self.cbs_by_id = set()
        self.checkboxes = set()
        self.line_pace = None
        self.line_pace_by_id = 0
        self.from_, self.to_ = (0, 0)
        self.sel_length = 0
        self.line_end = 0
        self.dic_lines = {}
        self.dic_entries = {}
        self.snd_file_list = []
        self.loaded_max = 0
        self.end = 0
        self.count_error = 0
        self.count_auto = 0
        self.hint = 'pacenotes'
        self.modified = False
        self.udp = False

        self.timer_error = wx.Timer(self)
        self.timer_auto = wx.Timer(self)

        self.taskbar = TaskBar(self)  # Create taskbar icon.

        self.menu_bar = MenuBar(self, self.GetName(), img_path, q_del)
        self.menu_bar.Append(self.menu_bar.file_menu, '&File')
        self.menu_bar.Append(self.menu_bar.edit_menu, '&Edit')
        self.menu_bar.Append(self.menu_bar.autosave_menu, '&Autosave')
        self.menu_bar.Append(self.menu_bar.delay_menu, '&Pacenotes')
        self.menu_bar.Append(self.menu_bar.help_menu, '&Help')
        self.SetMenuBar(self.menu_bar)
        self.menu_bar.EnableTop(1, False)
        self.menu_bar.EnableTop(2, False)
        self.menu_bar.EnableTop(3, False)

        self.statusbar = self.CreateStatusBar()
        self.statusbar.SetName('status')

        self.joy = JoyCatcher()

        self.dic_stages = defaultdict(list)
        self.read_stages_csv()
        self.scan_sound_folder()

        self.editor = Editor(self)

        self.persist_manager = per.PersistenceManager.Get()
        config_file = os.path.join(data_path, self.editor.tabs.GetName())
        self.persist_manager.SetPersistenceFile(config_file)
        self.persist_manager.RegisterAndRestore(self.editor.tabs)

        self.progress = wx.Gauge(self.statusbar, pos=(265, 4), range=self.loaded_max)

        # if self.mode == 'Record':
        # self.recorder = AudioRecorder()
        self.loader = AudioLoader(self.snd_file_list)  # load AudioSegments into RAM
        self.reader = UDP()
        # self.count_player = AudioPlayer()

        if not self.co_driver:  # First run.
            self.show_settings()

        if self.mode == 'Classic':
            self.reload_sounds()

        pub.subscribe(self.get_progress, 'get_progress')
        pub.subscribe(self.get_stage, 'get_stage')
        pub.subscribe(self.get_dist, 'get_dist')
        pub.subscribe(self.key_error, 'key_error')

        self.Bind(wx.EVT_TIMER, self.on_timer_error, self.timer_error)
        self.Bind(wx.EVT_TIMER, self.on_timer_auto, self.timer_auto)
        self.Bind(wx.EVT_CLOSE, self.on_quit)

        wx.CallAfter(self.register_controls)

    def get_progress(self, loaded):
        self.progress.SetValue(loaded)
        if loaded == self.loaded_max:
            self.progress.Destroy()
            self.SetStatusText('Open pacenotes file or start recce')

    def get_stage(self, stage_name, stage_path):
        if self.stage_name:
            if stage_name != self.stage_name and self.modified:
                dlg = wx.MessageDialog(self, 'Do you want to save ' + self.file_name + '?', 'Confirm',
                                       wx.YES_NO | wx.YES_DEFAULT | wx.ICON_QUESTION)
                if dlg.ShowModal() == wx.ID_YES:
                    self.write_file()
                    wx.MessageBox(self.file_name + ' has been saved', 'Confirmation',
                                  wx.OK | wx.ICON_INFORMATION)
        self.stage_name = stage_name
        self.stage_path = stage_path
        self.udp = True
        self.update_stage()

    def update_stage(self):
        self.file_name = self.stage_name + '.txt'
        self.open_file()
        self.menu_bar.EnableTop(3, True)
        self.menu_bar.menu_open.Enable(False)
        self.editor.button_play.Enable()
        self.update_delay()

    def get_dist(self, curr_dist, last_dist):
        self.curr_dist = curr_dist
        self.last_dist = last_dist
        self.update_dist()

    def key_error(self, arg):
        self.statusbar.SetStatusText('\'' + arg + '\'' + ' not found in ' + self.co_driver + '/sounds folder')
        self.error()

    def register_controls(self):
        self.Freeze()
        self.register()
        self.Thaw()

    def register(self, children=None):
        if children is None:
            self.persist_manager.RegisterAndRestore(self)
            children = self.GetChildren()
        for child in children:
            name1 = child.GetName()
            grandchildren = child.GetChildren()
            for grandchild in grandchildren:
                name2 = grandchild.GetName()

    def get_config(self):
        if not os.path.exists(config_ini):
            self.create_config()
        return ConfigObj(config_ini)

    def create_config(self):  # Set default values for config.ini.
        config = ConfigObj(config_ini)
        config['ip'] = '127.0.0.1'
        config['port'] = '20777'
        config['mode'] = 'Classic'
        config['co_driver_c'] = ''
        config['co_driver_r'] = ''
        config['delay'] = '200'
        config['interval'] = '1000'
        config['rec_key'] = 'r'
        config['han_key'] = 'h'
        config['countdown'] = 'True'
        config.write()

    def update_config(self):
        config = ConfigObj(config_ini)
        config['ip'] = self.ip
        config['port'] = self.port
        config['mode'] = self.mode
        config['co_driver_c'] = self.co_driver_c
        config['co_driver_r'] = self.co_driver_r
        config['delay'] = self.delay
        config['interval'] = self.interval
        config['rec_key'] = self.rec_key
        config['han_key'] = self.han_key
        config['countdown'] = self.countdown
        config.write()

    def show_creator(self):
        self.manager = Manager(self)
        self.manager.ShowModal()

    def show_settings(self):
        self.settings = Settings(self, self.GetName(), img_path)
        self.settings.ShowModal()

    def on_cancel(self, event):
        self.clear_input_pace()

    def read_stages_csv(self):
        try:
            with open(os.path.join(app_path, 'data\\stages.csv'), 'r') as f:
                _ = next(f)
                for line in f:
                    row = line.strip()
                    lis = row.partition(',')  # tuple
                    key = float(lis[0])  # key as float (distance)
                    val = lis[2]  # value as string (stage, country, province)
                    self.dic_stages[key].append(val)  # dictionary
        except FileNotFoundError:
            self.SetStatusText('stages.csv file not found')
            self.error()
        q_stg.put_nowait(self.dic_stages)

    def update_stage_name(self):
        try:
            for k, v in list(self.dic_stages.items()):
                for s in v:
                    stg = s.split(',')
                    if self.stage_name == stg[1]:
                        self.stage_name_full = stg[1] + ', ' + stg[3] + ', ' + stg[2]
                        self.editor.label_length.SetLabel(str(round(k)) + ' m')
                '''
                if len(v) == 1:
                    stg = s.split(',')
                    if self.stage_name == stg[1]:
                        self.stage_name_full = stg[1] + ', ' + stg[3] + ', ' + stg[2]
                elif len(v) == 2:
                    stg = s.split(',')
                    pos_start = int(stg[0])
                    if self.pos_y == pos_start:
                        self.stage_name_dic = stg[1]
                        self.stage_folder = stg[2]
                '''
            self.editor.box_pace.SetLabel(self.stage_name_full)
        except AttributeError:
            self.SetStatusText(self.stage_name + ' is not a valid stage')
            self.error()

    def update_dist(self):
        if self.curr_dist >= 0 and not self.editor.input_dist.HasFocus():
            self.editor.input_dist.SetValue(self.curr_dist)

        # Manage scrolling.
        sort_keys = sorted(list(self.dic_lines.keys()), key=int)
        lines = []
        for index, d in enumerate(sort_keys):
            lines.append(index)
            if index < (len(sort_keys)):
                self.curr_line = self.dic_lines[sort_keys[index]]
                self.prev_line = self.dic_lines[sort_keys[index - 1]]
            if self.curr_dist > self.last_dist:
                if self.curr_dist == int(d) - (self.delay - 100):
                    # print self.editor.scrolled_panel.GetScrollPos(wx.VERTICAL), 'pos'
                    # print self.editor.scrolled_panel.GetScrollLines(wx.VERTICAL), 'lines'
                    self.curr_line.SetFont(self.font.Bold())
                    self.prev_line.SetFont(self.font)
                    # print index, d, 'index'  # TODO
                    if index > 1:
                        self.editor.scrolled_panel.ScrollLines(3)
                        self.Refresh()
            # elif self.curr_dist < self.last_dist:  # If going wrong way.
            #     self.curr_line.SetFont(self.font)
            #     self.prev_line.SetFont(self.font)
            #     self.Refresh()
            elif self.curr_dist == 0:  # If at the start line.
                self.editor.scrolled_panel.Scroll(0, 0)
                for line in lines:
                    self.curr_line = self.dic_lines[sort_keys[line]]
                    self.curr_line.SetFont(self.font)
                self.Refresh()

    def update_delay(self):
        if self.delay == 100:
            self.delay_mode = 'RECCE'
        elif self.delay == 150:
            self.delay_mode = 'STAGE (Late)'
        elif self.delay == 200:
            self.delay_mode = 'STAGE (Normal)'
        elif self.delay == 250:
            self.delay_mode = 'STAGE (Earlier)'
        elif self.delay == 300:
            self.delay_mode = 'STAGE (Very Early)'
        self.editor.label_delay.SetLabel(self.delay_mode)

    def create_pacenotes(self):
        h_box_scr = wx.BoxSizer(wx.HORIZONTAL)

        tick = wx.CheckBox(self.editor.scrolled_panel, id=int(self.dist), name='tick')
        self.Bind(wx.EVT_CHECKBOX, self.on_tick)
        h_box_scr.Add(tick, 0, wx.ALIGN_CENTER_VERTICAL | wx.LEFT, 5)
        self.checkboxes.add(tick)

        text_dist = intctrl.IntCtrl(self.editor.scrolled_panel, id=self.dist, name='dist', value=self.dist,
                                    min=-19999, max=19999, size=wx.Size(45, 23),
                                    style=wx.TE_PROCESS_ENTER, limited=True, allow_none=False)
        text_dist.Bind(wx.EVT_TEXT_ENTER, self.on_distance)
        h_box_scr.Add(text_dist, 0, wx.LEFT | wx.RIGHT, 1)

        text_pace = wx.TextCtrl(self.editor.scrolled_panel, id=self.dist, name='pace', value=self.pace)
        text_pace.Bind(wx.EVT_MOUSE_CAPTURE_CHANGED, self.on_selection)
        h_box_scr.Add(text_pace, 1, wx.EXPAND | wx.RIGHT, 5)
        text_pace.SetEditable(False)
        text_pace.SetCursor(wx.Cursor(wx.CURSOR_ARROW))

        self.editor.v_box.Add(h_box_scr, 0, wx.EXPAND | wx.BOTTOM, 1)

        self.editor.scrolled_panel.Layout()
        self.editor.scrolled_panel.FitInside()
        self.end = self.editor.scrolled_panel.GetScrollLines(wx.VERTICAL)
        self.dic_lines[self.dist] = text_pace
        self.dic_entries[self.dist] = self.pace.strip('\n')

    def reload_pacenotes(self):
        self.editor.scrolled_panel.DestroyChildren()
        self.checkboxes.clear()
        for dist in sorted(self.dic_entries, key=int):
            self.dist = int(dist)
            self.pace = self.dic_entries[dist].strip('\n')
            self.create_pacenotes()
        q_dic.put_nowait(self.dic_entries)
        self.modified = True

    def reload_sounds(self):
        self.read_sounds_csv()
        self.editor.tabs.DeleteAllPages()
        for category, sounds_list in list(self.sound_list.items()):
            tab = wx.Panel(self.editor.tabs, name=category)
            tab.SetCursor(wx.Cursor(wx.CURSOR_HAND))
            list_ctrl = ulc.UltimateListCtrl(tab, agwStyle=ulc.ULC_BORDER_SELECT | ulc.ULC_SORT_ASCENDING
                                                           | ulc.ULC_SINGLE_SEL | ulc.ULC_HOT_TRACKING | wx.LC_LIST)
            for index, sound in enumerate(sounds_list):
                list_ctrl.InsertStringItem(index, sound)

            h_box_tabs = wx.BoxSizer(wx.HORIZONTAL)
            h_box_tabs.Add(list_ctrl, 1, wx.EXPAND)
            tab.SetSizerAndFit(h_box_tabs)
            self.editor.tabs.AddPage(tab, category)

        self.Bind(wx.EVT_LIST_ITEM_SELECTED, self.on_pacenote)

        if not self.stage_name:
            self.editor.tabs.Disable()
        # self.editor.Refresh()

    def scan_sound_folder(self):  # TODO - separate thread?
        if self.mode == 'Classic':
            self.snd_file_list = glob.glob(self.sound_path + '/*')  # all sound files
        elif self.mode == 'Record':
            for entry in os.scandir(self.sound_path):  # only generic sound files
                if entry.is_file():
                    self.snd_file_list.append(entry.path)
            # self.snd_file_list = [os.path.join(self.sound_path, fn) for fn in next(os.walk(self.sound_path))[2]]
        print(self.snd_file_list)
        self.loaded_max = len(self.snd_file_list)
        self.statusbar.SetStatusText('Processing audio files, please wait...')

    def read_sounds_csv(self):
        if self.mode == 'Classic':
            try:
                self.sound_list.clear()
                with open(self.sounds_csv, 'r') as csv_file:
                    csv_data = csv.DictReader(csv_file)
                    for row in csv_data:
                        pair = list(row.items())  # list of tuples of (key, value) pairs
                        for key, value in pair:
                            if value:
                                self.sound_list[key].append(value)  # dict with multiple values for same keys
            except FileNotFoundError:
                wx.MessageBox(self.co_driver + ' is missing sounds.csv file', 'CO-DRIVER ERROR', wx.OK | wx.ICON_ERROR)
                self.on_quit(None)
        elif self.mode == 'Record':
            try:
                self.sound_list.clear()
                # print(self.co_dir)
                with open(self.sounds_csv, 'r') as csv_file:
                    csv_data = csv.DictReader(csv_file)
                    for row in csv_data:
                        pair = list(row.items())  # list of tuples of (key, value) pairs
                        for key, value in pair:
                            if value:
                                self.sound_list[key].append(value)  # dict with multiple values for same keys
            except FileNotFoundError:
                wx.MessageBox(self.co_driver + ' is missing sounds.csv file', 'CO-DRIVER ERROR', wx.OK | wx.ICON_ERROR)
                self.on_quit(None)

    def error(self):
        self.statusbar.SetBackgroundColour('RED')
        self.statusbar.Refresh()
        self.timer_error.Start(50)

    def on_timer_error(self, event):
        self.count_error = self.count_error + 1
        if self.count_error == 25:
            self.statusbar.SetBackgroundColour('white')
            self.statusbar.Refresh()
            self.timer_error.Stop()
            self.count_error = 0

    def on_timer_auto(self, event):
        self.count_auto = self.count_auto + 1
        if self.count_auto == self.interval:
            self.write_file()
            self.SetStatusText('File ' + self.file_name + ' has been auto-saved.')
            self.autosave()

    def on_interval(self, event):
        self.timer_auto.Stop()
        evt = event.GetEventObject()
        self.interval = event.GetId()
        self.autosave()

    def clear_input_pace(self):
        self.editor.input_pace.Clear()
        self.editor.input_pace.SetHint(self.hint)
        self.editor.input_dist.SetFocus()
        for button in self.editor.buttons:
            button.Disable()
        self.editor.button_play.Disable()

    def on_add(self, event):
        self.dist = self.editor.input_dist.GetValue()
        if self.stage_name:
            if self.dist != 0:
                for dist in self.dic_entries:
                    if self.dist == dist:
                        dlg = wx.MessageDialog(self, 'Replace pacenotes for current distance?', 'Confirm',
                                               wx.YES_NO | wx.NO_DEFAULT | wx.ICON_QUESTION)
                        dlg_choice = dlg.ShowModal()
                        if dlg_choice == wx.ID_YES:
                            self.add_pacenotes()
                            sort_keys = sorted(list(self.dic_lines.keys()), key=int)
                            for index, d in enumerate(sort_keys):
                                if self.dist == d:
                                    self.editor.scrolled_panel.Scroll(0, index)
                            self.SetStatusText('Pacenotes replaced')
                            return
                        elif dlg_choice == wx.ID_NO:
                            dlg.Destroy()
                            self.SetStatusText('Operation cancelled')
                            return
                self.add_pacenotes()
                sort_keys = sorted(list(self.dic_lines.keys()), key=int)
                for index, d in enumerate(sort_keys):
                    if self.dist == d:
                        self.editor.scrolled_panel.Scroll(0, index)
                self.SetStatusText('Pacenotes added')
            else:
                self.SetStatusText('Distance cannot be 0')
                self.error()
        else:
            self.SetStatusText('Open pacenotes text file or run a stage first')
            self.error()

    def add_pacenotes(self):
        self.pace = self.editor.input_pace.GetValue()
        self.dic_entries[self.dist] = self.pace.strip('\n')
        self.reload_pacenotes()
        self.editor.button_add.Disable()
        self.editor.button_insert.Disable()
        self.editor.button_replace.Disable()
        self.clear_input_pace()

    def on_insert(self, event):
        if self.sel_length == 0 == self.from_:  # Insert pacenote at the beginning of line.
            self.line_pace.SetInsertionPoint(self.from_)
        else:  # Insert pacenote after selection.
            self.line_pace.SetInsertionPoint(self.to_)
        self.line_pace.WriteText(self.editor.input_pace.GetValue())
        self.dic_entries[self.line_pace_by_id] = self.line_pace.GetValue().replace('\n', '')
        self.reload_pacenotes()
        self.editor.button_add.Disable()
        self.editor.button_insert.Disable()
        self.editor.button_replace.Disable()
        self.editor.button_delete.Disable()
        self.clear_input_pace()
        self.editor.button_play.Disable()
        sort_keys = sorted(list(self.dic_lines.keys()), key=int)
        for index, d in enumerate(sort_keys):
            if self.line_pace_by_id == int(d):
                self.editor.scrolled_panel.Scroll(0, index)
        self.SetStatusText('Pacenote inserted')

    def on_replace(self, event):
        self.line_pace.Replace(self.from_, self.to_, self.editor.input_pace.GetValue())
        self.dic_entries[self.line_pace_by_id] = self.line_pace.GetValue().strip('\n')
        self.reload_pacenotes()
        self.editor.button_add.Disable()
        self.editor.button_insert.Disable()
        self.editor.button_replace.Disable()
        self.editor.button_delete.Disable()
        self.clear_input_pace()
        self.editor.button_play.Disable()
        sort_keys = sorted(list(self.dic_lines.keys()), key=int)
        for index, d in enumerate(sort_keys):
            if self.line_pace_by_id == int(d):
                self.editor.scrolled_panel.Scroll(0, index)
        self.SetStatusText('Pacenote replaced')

    def on_delete(self, event):
        if self.cbs_by_id:  # Remove checked lines.
            for dist in self.cbs_by_id:
                del self.dic_entries[dist]
                del self.dic_lines[dist]
            self.cbs.clear()
            self.cbs_by_id.clear()
            self.checkboxes.clear()
            self.editor.button_delete.Disable()
            self.menu_bar.menu_select_all.Check(False)
        else:  # Remove selected text.
            self.line_pace.Remove(self.from_, self.to_)
            dic_2: ast.Dict[int, Any] = {self.line_pace_by_id: self.line_pace.GetValue().strip('\n')}
            self.dic_entries.update(dic_2)
        self.reload_pacenotes()
        self.clear_input_pace()
        self.editor.button_play.Disable()
        sort_keys = sorted(list(self.dic_lines.keys()), key=int)
        for index, d in enumerate(sort_keys):
            if self.line_pace_by_id == int(d):
                self.editor.scrolled_panel.Scroll(0, index)
        self.SetStatusText('Pacenote deleted')

    def on_selection(self, event):
        self.line_pace = event.GetEventObject()
        line_pace_by_name = self.line_pace.GetName()
        self.line_pace_by_id = self.line_pace.GetId()
        self.from_, self.to_ = self.line_pace.GetSelection()
        self.line_end = self.line_pace.GetLastPosition()
        self.sel_length = self.to_ - self.from_
        if self.cbs:  # Clear any ticks.
            for cb in self.cbs:
                cb.SetValue(False)
            self.cbs.clear()
            self.cbs_by_id.clear()
        else:
            if line_pace_by_name == 'pace':
                if self.sel_length > 0:
                    if self.editor.input_pace.GetValue():
                        self.editor.button_insert.Enable()
                        self.editor.button_replace.Enable()
                        self.editor.button_delete.Enable()
                    else:
                        self.editor.button_insert.Disable()
                        self.editor.button_replace.Disable()
                        self.editor.button_delete.Enable()
                elif self.sel_length == 0 == self.from_:
                    self.editor.button_insert.Enable()
                    self.editor.button_replace.Disable()
                    self.editor.button_delete.Disable()
                else:  # Prevent splitting words.
                    self.editor.button_insert.Disable()
                    self.editor.button_replace.Disable()
                    self.editor.button_delete.Disable()

    def on_distance(self, event):
        line_dist = event.GetEventObject()
        line_dist_by_name = line_dist.GetName()
        line_dist_by_id = line_dist.GetId()
        self.dist = line_dist.GetValue()
        if self.dist:
            if line_dist_by_name == 'dist':  # Processed by Enter.
                if self.dist != line_dist_by_id:
                    self.dic_entries[self.dist] = self.dic_entries.pop(line_dist_by_id, '')
                    self.dic_lines[self.dist] = self.dic_lines.pop(line_dist_by_id, '')
                    self.reload_pacenotes()
                    sort_keys = sorted(list(self.dic_lines.keys()), key=int)
                    for index, d in enumerate(sort_keys):
                        if self.line_pace_by_id == int(d):
                            self.editor.scrolled_panel.Scroll(0, index)
                    self.SetStatusText('Distance updated')
                else:
                    self.statusbar.Refresh()
            elif line_dist_by_name == 'input':
                if self.editor.input_pace.GetValue():
                    self.editor.button_add.Enable()
                    self.editor.button_play.Enable()
                else:
                    self.editor.button_add.Disable()
                    self.editor.button_play.Enable()

    def on_pacenote(self, event):
        if not self.editor.input_pace.GetValue():  # Get rid of pacenote hint.
            self.editor.input_pace.Clear()
            self.editor.button_play.Enable()
        self.editor.input_pace.AppendText(event.GetText() + ' ')
        if self.line_pace:  # If text selected.
            if self.editor.input_dist.GetValue():
                self.editor.button_add.Enable()
            self.editor.button_insert.Enable()
            self.editor.button_replace.Enable()
        elif not self.line_pace:
            if self.editor.input_dist.GetValue():
                self.editor.button_add.Enable()
                self.editor.button_insert.Disable()
                self.editor.button_replace.Disable()
            else:
                for button in self.editor.buttons:
                    button.Disable()

    def on_tick(self, event):
        self.editor.button_insert.Disable()
        self.editor.button_replace.Disable()
        cb = event.GetEventObject()
        cb_by_id = event.GetId()
        if cb_by_id != 20000:
            if cb.IsChecked():
                self.cbs.add(cb)
                self.cbs_by_id.add(cb_by_id)
                self.editor.button_delete.Enable()
                self.menu_bar.menu_select_all.IsChecked()
            else:
                self.cbs.remove(cb)
                self.cbs_by_id.remove(cb_by_id)
            if not self.cbs:
                self.editor.button_delete.Disable()
            self.menu_bar.menu_select_all.Check(False)

        else:  # Select All.
            for tick in self.checkboxes:
                tick_by_id = tick.GetId()
                if self.menu_bar.menu_select_all.IsChecked():
                    self.cbs.add(tick)
                    self.cbs_by_id.add(tick_by_id)
                    tick.SetValue(True)
                    self.editor.button_delete.Enable()
                else:
                    self.cbs.clear()
                    self.cbs_by_id.clear()
                    tick.SetValue(False)
                    self.editor.button_delete.Disable()

    def on_undo_select(self, event):
        # stock_undo = []
        # undo = self.text_pace.Undo()
        # stock_undo.append(undo)
        pass

    def on_about(self, event):
        description = wordwrap('DiRTy Pacenotes is a tool for creating your own pacenotes\n'
                               'for DiRT Rally and DiRT Rally 2.0 special stages.\n', 420, wx.ClientDC(self))
        licence = wordwrap('Licensed under the Apache License, Version 2.0 (the "License");\n'
                           'you may not use this software except in compliance with the License.\n'
                           'You may obtain a copy of the License at\n'
                           'http://www.apache.org/licenses/LICENSE-2.0\n'
                           'Unless required by applicable law or agreed to in writing,\n'
                           'software distributed under the License is distributed on an "AS IS" BASIS,\n'
                           'WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,\n'
                           'either express or implied. See the License for the specific language\n'
                           'governing permissions and limitations under the License.', 420, wx.ClientDC(self))
        info = wx.adv.AboutDialogInfo()
        info.SetName('DiRTy Pacenotes')
        info.SetVersion('2.9.5')
        info.SetDescription(description)
        info.SetCopyright('(C) 2017 - 2020 Palo Samo')
        info.SetLicence(licence)
        wx.adv.AboutBox(info)

    def autosave(self):
        self.count_auto = 0
        if self.interval == 1000:
            self.SetStatusText('Autosave OFF')
        else:
            self.timer_auto.Start(60000)
            self.SetStatusText('Autosave set to ' + str(self.interval) + ' minutes')

    def on_quit(self, event):
        if self.stage_name and self.modified:
            dlg = wx.MessageDialog(self, 'Do you want to save ' + self.file_name + '?', 'Confirm',
                                   wx.YES_NO | wx.YES_DEFAULT | wx.ICON_WARNING)
            dlg_choice = dlg.ShowModal()
            if dlg_choice == wx.ID_YES:
                self.write_file()
                dlg = wx.MessageDialog(self, self.file_name + ' has been saved', 'Confirmation',
                                       wx.OK | wx.ICON_INFORMATION)
                dlg.ShowModal()
        if not self.stage_name:  # from 'Create your co-driver'
            pass
        self.persist_manager.SaveAndUnregister(self.editor.tabs)
        pub.unsubAll()
        udp_running = False
        q_run.put_nowait(udp_running)
        self.update_config()
        self.taskbar.Destroy()
        self.Destroy()

    def open_file(self):
        self.editor.scrolled_panel.DestroyChildren()
        self.dic_entries.clear()
        self.dic_lines.clear()
        file_handle = os.path.join(self.stage_path, self.file_name)
        try:
            with open(file_handle, 'r') as f:
                for line in f:
                    if line and line.split():
                        lis = line.partition(',')  # tuple
                        self.dist = int(lis[0])
                        self.pace = lis[2]
                        self.create_pacenotes()
                    else:
                        continue
        except FileNotFoundError:
            self.SetStatusText(self.file_name + ' not found in ' + self.co_driver + '\'s pacenotes folder')
            self.error()
            return
        self.menu_bar.menu_save.Enable(True)
        self.menu_bar.EnableTop(1, True)
        self.menu_bar.EnableTop(2, True)
        self.editor.input_pace.Clear()
        self.editor.input_pace.SetHint(self.hint)
        self.editor.tabs.Enable()
        self.editor.input_dist.Enable()
        self.editor.button_play.Disable()
        for button in self.editor.buttons:
            button.Disable()
        self.update_stage_name()
        self.update_delay()
        # self.reload_sounds()
        self.modified = False
        self.autosave()

    def write_file(self):
        self.file_handle = os.path.join(self.stage_path, self.file_name)
        with open(self.file_handle, 'w') as f:
            for dist in sorted(self.dic_entries, key=int):
                pace = self.dic_entries[dist]
                line = '{},{}'.format(dist, pace)
                f.write(line + '\n')
        self.modified = False

    def on_restart(self, event):  # gather data, update config file and restart app
        if self.settings.ip_value.GetValue():
            self.ip = self.settings.ip_value.GetValue()
        else:
            wx.MessageBox('IP is missing', 'MISSING VALUE', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.port_value.GetValue():
            self.port = self.settings.port_value.GetValue()
        else:
            wx.MessageBox('Port is missing', 'MISSING VALUE', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.combo_co_driver.GetValue():
            self.co_driver = self.settings.combo_co_driver.GetValue()
        else:
            wx.MessageBox('Co-Driver is missing', 'VALUE MISSING', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.rec_key_value.GetValue():
            self.rec_key = self.settings.rec_key_value.GetValue()
        else:
            wx.MessageBox('Change Record key/button', 'WRONG VALUE', wx.OK | wx.ICON_WARNING)
            return
        if self.settings.rec_key_value.GetValue():
            self.han_key = self.settings.han_key_value.GetValue()
        else:
            wx.MessageBox('Change Handbrake key/button', 'WRONG VALUE', wx.OK | wx.ICON_WARNING)
            return
        self.countdown = self.settings.count_check.GetValue()

        if self.mode == 'Classic':
            self.co_driver_c = self.co_driver
        elif self.mode == 'Record':
            self.co_driver_r = self.co_driver

        if self.co_driver:
            self.on_quit(event)
            self.restart_app(self)
        else:  # First run.
            if not self.co_driver:
                wx.MessageBox('Choose or create a co-driver', 'CO-DRIVER OPTION', wx.OK | wx.ICON_WARNING)
                return
            self.update_config()
            self.settings.Destroy()
            self.on_quit(event)

    @staticmethod
    def restart_app(self):
        sys.stdout.flush()
        os.execl(sys.executable, sys.executable, *sys.argv)

    # TODO - read generic sounds and sounds from stages into RAM in separate threads at startup
    # TODO - sounds when stationary car on stage
    # TODO - sounds when stage is finished
    # TODO - pump up the volume?
    # TODO - app crashes when distance is edited manually in pacenotes
    # TODO - stop playback when not needed
    # TODO - no pacenotes table for stage mode


if __name__ == '__main__':
    app = wx.App()
    frame = DiRTyPacenotes(None)
    frame.Centre()
    frame.Show()
    app.MainLoop()
